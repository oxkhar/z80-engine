# Z80 Assembly Programming Style Guide (Using PASMO Compiler)

PASMO is the assembler used in this project, this is the
[PASMO Documentation](https://pasmo.speccy.org/pasmodoc.html)

## Table of Contents

1. [Basic Formatting](#basic-formatting)
2. [Naming Conventions](#naming-conventions)
3. [Comments](#comments)
4. [Macros](#macros)
5. [Data and Variables](#data-and-variables)
6. [Control Flow](#control-flow)
7. [Code Organization](#code-organization)

## Basic Formatting

### Indentation and Spacing

* Use spaces, not tabs
* Instructions must be indented with 4 spaces
* Include a space between parameters (e.g., `ld a, b`)
* Keep code aligned with consistent indentation for each level
* "IF" has indentation of 2 spaces
* Use blank lines to separate logical blocks of code

### Directives and Instructions

* Directives must be in uppercase
* All assembly instructions must be in lowercase
* Functions must be ended with a colon

### File Structure

* Use `.h.asm` extension for header files containing macros and constants
  * All macros must be defined before they are used
  * All macros must be defined in this file
  * All macros must be grouped in a file together to define a package
  * Macros must be prefixed with the name of the package and an underscore
* Use `.asm` extension for source files containing assembly code
  * Include required files at the beginning of the source file
  * Group related functions and data structures together to define a package
  * Functions must be prefixed with the name of the package and an underscore

#### Example of File Structure

```asm
INCLUDE "required.h.asm"

; Constants
CONST_NAME EQU value

; Data structures
PackageName_$structName:
    DB value

; Code sections
PackageName_FunctionName:
    ; Function implementation
```

## Naming Conventions

### General Rules

* Use descriptive English names that reflect functionality
* Follow case conventions for different elements:
  * UPPERCASE for constants and assembler directives
  * lowerCamelCase for variables
  * _UpperCamelCase for local functions
  * UpperCamelCase for public functions and structures
  * PackageName_ prefix for all elements in a package
  * $lowerCamelCase for labels that define memory addresses to be like variables

### Specific Elements for ASM files (.asm)

* Functions:
  * Public: UpperCamelCase with colon (e.g., `DrawScore:`)
  * Local: lowerCamelCase with colon (e.g., `_CalculateNextFrameTime:`)
* Variables/Structures:
  * All start with `$`
  * Regular: lowerCamelCase (e.g., `$playerScore: DW 0`)
  * Some special types:
    * Pointers: Start with 'p' (e.g., `$pActivePlayer: DW 0x0000`, `$pNextSprite: DW 0x0000`)
    * Counters: Start with 'n' (e.g., `$nLives: DB 3`, `$nBullets: DB 0`)
    * Flags: Start with 'f' (e.g., `$fGameOver: DB 0`, `$fPaused: DB 0`)
    * Arrays: End with 's' (e.g., `$enemies: DS 10`, `$scores: DS 5`)

### Specific Elements for macros files (.h.asm)

* All macros start with `@`
* Parameters start with `.` (dot)
* Labels start with `.`

### Package Prefixing

* Prefix macro names with package name: `@PackageName_MacroName`
* Private macros use double underscore: `@PackageName__PrivateMacro`
* Prefix labels with package name: `.PackageName_labelName`
* Prefix functions with package name: `PackageName_FunctionName`
* Prefix data structures with package name: `PackageName_$structName`
* Prefix variables with package name: `PackageName_$varName`
* Constants are not prefixed: `CONST_NAME EQU value`

## Comments

### Style

* Use semicolon (`;`) for all comments
* Use uppercase for register names in comments
* Use lowercase for other comment
* No vertical space between function comment and function
* Document all parameters and return values
  * Parameters has `=`
  * Return values has `<-`
  * Registers modified has header `Modifies:`

### Function Documentation

Example Function with Documentation...

```asm
;;
; Copies a block of memory from source to destination
;   HL = Source address
;   DE = Destination address
;   BC = Bytes to copy
;   A <- 0 if successful, 1 if error
; Modifies:
;   F, BC, DE, HL
Memory_Copy:
    ld a, b         ; Check if count is zero
    or c
    ret z           ; Return if nothing to copy
    
    ldir            ; Copy bytes
    xor a           ; Return success
    ret
```

### Macro Documentation

Example Macro with Documentation...

```asm
;;
; Creates a new sprite with animation capabilities
; Parameters:
;   IX = Sprite address
;   .name    = Sprite identifier
;   .frames  = Number of animation frames
;   .speed   = Animation speed (0-255)
@Sprite_Create MACRO .name, .frames, .speed
    [macro content here]
ENDM
```

## Macros

### Naming and Structure

* Prefix with `@` for high-level functions
* Use UpperCamelCase after prefix
* Document purpose and parameters
* Macro parameters start with `.` (dot)
* Labels start with `.`
* Labels within macros can start with `_`
* Use `##` operator for concatenation parameters in macros

#### Example of Macro with Parameters

```asm
;;
; Create a new stack with specified name and size
;   .name: Name of the stack
;   .size: Maximum number of items
@Stack_Create MACRO .name, .size
    
Stack_$begin ## .name:
    DS .size
Stack_$end ## .name:
ENDM

@Stack_Push MACRO .name
    ld hl, Stack_$begin ## .name
    call Stack_PushItem
ENDM
```

### Label Definitions in Macros

* Use `.` prefix for labels that need to be unique within macro expansions
* Use descriptive suffixes after the macro parameter
* Example: `.Stack_Begin`, `.Loop_Start`

```asm
;; Labels
.posX DEFL 0
.Screen_Address DEFL 0x0000
```

#### Example of Complete Macro Structure

```asm
;;
; Creates a new entity at specified coordinates
;   .name = Entity identifier
;   .x = Initial X position
;   .y = Initial Y position
@Entity_Create MACRO .name, .x, .y
   
Entity ## .name:
    DB .x          ; X position
    DB .y          ; Y position
    
Entity ## .name ## _Update:
    ; Update logic here
ENDM

@PackageName__PrivateNameFunction MACRO .param, .otherParam

ENDM

```

## Data and Variables

### Constants and Literals

* Use `0x` prefix for hexadecimal values
* Use meaningful names for constants
* Document data structures and their purpose

#### Example of Constants and Data Structures

```asm
;; Constants
STACK_SIZE   EQU 0x20        ; Maximum stack size
BASE_ADDR    EQU 0x4000      ; Screen memory base address

;; Data structures
Package_$playerData:
    DB 0x00                 ; Player status
    DW 0x4000               ; Player position
```

## Control Flow

### Labels and Jumps

* Use descriptive names for loops and jump points
* Prefix package-specific labels appropriately
* Use `jr` for short jumps and `jp` for long jumps

#### Example of Labels and Jumps

```asm
_loopBegin:
    ; Loop logic here
    cp STACK_END
    jr nz, _loopContinue
    jp _loopExit
```

---

For more examples and detailed guidelines, see the example files in the project repository.
