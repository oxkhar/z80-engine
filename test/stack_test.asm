INCLUDE "test.h.asm"
;
INCLUDE "stack.h.asm"
;
@Test_Begin

INCLUDE "stack.asm"
;;;
;; See stack in memory 0x5250
;; Elements are in 0x5301, ...

Test_SetUp:
    @Stack_Reset StackForTestingEnd
    @Stack_Reset StackTestItems
    @Stack_Reset StackOther
    ld hl, items
IRP .item, 0x01, 0x02, 0x03, 0x04, 0x05  ; reset values of items
    ld [hl], .item
    inc hl
ENDM
    ret

@Test_Exec should_last_item_point_to_end_of_stack

    @Test_AssertAddressContainsWord StackForTestingEnd, StackForTestingEnd_EndStack

    ld ix, itemTwo
    @Stack_AddItem StackForTestingEnd
    @Test_AssertAddressContainsWord (StackForTestingEnd + 2), StackForTestingEnd_EndStack
    @Test_AssertAddressContainsWord itemTwo, StackForTestingEnd

@Test_Exec reset_stack
    ld hl, StackForTestingEnd
    call Stack_Reset

    @Test_AssertAddressContainsWord StackForTestingEnd, StackForTestingEnd_EndStack
    @Test_AssertAddressContainsWord STACK_END, StackForTestingEnd

@Test_Exec stack_write
IRP .item, itemOne, itemTwo, itemFive, itemFour, itemThree
    ld ix, .item
    @Stack_AddItem StackTestItems
ENDM

    @Test_AssertAddressContainsWord itemOne, (StackTestItems)
    @Test_AssertAddressContainsWord itemTwo, (StackTestItems + 2)
    @Test_AssertAddressContainsWord itemThree, (StackTestItems + 8)

    ld ix, itemFour
    @Stack_RemoveItem StackTestItems

    ld ix, itemFive
    @Stack_RemoveItem StackTestItems

    ; assert that stack has items 1, 2, 3
    @Test_AssertAddressContainsWord itemOne, (StackTestItems)
    @Test_AssertAddressContainsWord itemTwo, (StackTestItems + 2)
    @Test_AssertAddressContainsWord itemThree, (StackTestItems + 4)
    @Test_AssertAddressContainsWord STACK_END, (StackTestItems + 6)

@Test_Exec stack_with_one_item
    ld ix, itemTwo
    @Stack_AddItem StackOther

    @Test_AssertAddressContainsWord itemTwo, StackOther

    ld ix, itemTwo
    @Stack_RemoveItem StackOther

    ; assert that stack is empty
    @Test_AssertAddressContainsWord STACK_END, StackOther

@Test_Exec stack_walker
    ld ix, itemOne   ; with value 1
    @Stack_AddItem StackTestItems
    ld ix, itemTwo   ; with value 2
    @Stack_AddItem StackTestItems
    ld ix, itemFour  ; with value 4
    @Stack_AddItem StackTestItems

    @Stack_Walker stackLoopTest, StackTestItems, TestStack_WalkerPlusOne

    ; assert that elements with value 1, 2, 4 now are equals to 2, 3, 5
    @Test_AssertAddressContains 2, itemOne
    @Test_AssertAddressContains 3, itemTwo
    @Test_AssertAddressContains 5, itemFour

@Test_Exec stack_finder
    ld ix, itemOne
    @Stack_AddItem StackTestItems
    ld ix, itemTwo
    @Stack_AddItem StackTestItems
    ld ix, itemFive
    @Stack_AddItem StackTestItems
    ld ix, itemThree
    @Stack_AddItem StackTestItems

    @Stack_Finder stackLoopNotFound, StackTestItems, TestStack_FinderNotFound
    @Test_AssertEquals false, a

    @Stack_Finder stackLoopFound, StackTestItems, TestStack_FinderFound
    @Test_AssertEquals true, a

@Test_Exec stack_empty
    ld ix, itemOne
    @Stack_AddItem StackTestItems
    ld ix, itemTwo
    @Stack_AddItem StackTestItems

    ld hl, StackTestItems
    call Stack_Reset

    ; assert stack is empty (0xFFFF in first item)
    @Test_AssertAddressContains STACK_END, StackTestItems

@Test_Exec stack_empty_and_fill_again
IRP .item, itemOne, itemTwo, itemThree, itemFour, itemFive
    ld ix, .item
    @Stack_AddItem StackTestItems
ENDM

    ld hl, StackTestItems
    call Stack_Reset

    ld ix, itemFive
    @Stack_AddItem StackTestItems
    ld ix, itemFour
    @Stack_AddItem StackTestItems
    ld ix, itemThree
    @Stack_AddItem StackTestItems
    ld ix, itemTwo
    @Stack_AddItem StackTestItems
    ld ix, itemOne
    @Stack_AddItem StackTestItems

    ; assert stack contains items 5, 4, 3, 2, 1
    @Test_AssertAddressContainsWord itemFive, (StackTestItems)
    @Test_AssertAddressContainsWord itemFour, (StackTestItems + 2)
    @Test_AssertAddressContainsWord itemThree, (StackTestItems + 4)
    @Test_AssertAddressContainsWord itemTwo, (StackTestItems + 6)
    @Test_AssertAddressContainsWord itemOne, (StackTestItems + 8)

@Test_Exec stack_recover_last_item
    ld ix, itemFive
    @Stack_AddItem StackTestItems
    ld ix, itemFour
    @Stack_AddItem StackTestItems

    ld ix, itemThree
    @Stack_AddItem StackTestItems
    @Stack_LastItem StackTestItems

    @Test_AssertEquals itemThree, ix

    ld ix, itemOne
    @Stack_AddItem StackTestItems
    @Test_AssertEquals itemOne, ix

@Test_End

;; Auxiliar functions
TestStack_WalkerPlusOne:
    inc [ix + 0]
    @Stack_AddItem StackOther
    ret

TestStack_FinderFound:
    ld a, [itemFive]
    cp [ix + 0]
    jp z, _stackFoundFive
        ld a, false
        ret
_stackFoundFive:
    ld a, true
    ret

TestStack_FinderNotFound:
    ld a, [itemFour]
    cp [ix + 0]
    jp z, _stackFoundButNothing
        ld a, false
        ret
_stackFoundButNothing:
    ld a, true
    ret

;;
; Stack
ORG 0x5250
@Stack_Create StackTestItems, 7, 0xBE
@Stack_Create StackOther, 7
@Stack_Create StackForTestingEnd, 3

ORG 0x5301
items:
itemOne:
    DB  0x01
itemTwo:
    DB  0x02
itemThree:
    DB  0x03
itemFour:
    DB  0x04
itemFive:
    DB  0x05
