INCLUDE "test.h.asm"
;
@Test_Begin

INCLUDE "draw.asm"
;
Test_SetUp_draw_byte:
Test_SetUp_draw_pattern_vs_copy_screen:
Test_SetUp_draw_byte_vs_copy_screen:
    call Test_ClearScreen
Test_SetUp:
    call Test_DrawBorderRules
    ret

Test_SetUp_calculate_previous_line:
    call Test_ClearScreen
    ret

@Test_Wait copy_screen_128K
@Test_Wait copy_screen_amstrad_head
@Test_Wait copy_screen_amstrad_cosumer_electronics
@Test_Wait calculate_previous_line
@Test_Wait draw_byte
@Test_Wait draw_byte_with_gap
@Test_Wait draw_image
@Test_Wait apply_mask_and_draw_image
@Test_Wait draw_mask_in_image
@Test_Wait draw_and_copy_image
@Test_Wait draw_pattern
@Test_Wait draw_pattern_vs_copy_screen
@Test_Wait draw_byte_vs_copy_screen

@Test_End

copy_screen_128K:
    @Debug_Border 02
    @ld_xy hl, 18, 8
    @ld_xy de, 32, 75
    ld bc, 0x0808
    call Draw_CopyScreen

    ret

copy_screen_amstrad_head:
    @Debug_Border 03
    @ld_xy hl, 2, 8
    @ld_xy de, 1, 100
    ld bc, 0x3328
    call Draw_CopyScreen

    ret

copy_screen_amstrad_cosumer_electronics:
    @Debug_Border 08
    @ld_xy hl, 12, 24
    @ld_xy de, 20, 150
    ld bc, 0x3a16
    call Draw_CopyScreen

    ret

calculate_previous_line:
    @Debug_Border 02

    @ld_xy hl, 0, 0
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xF0
    ld a, 0x0F

    @ld_xy hl, 79, 0
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xf0
    ld a, 0x0f

    @ld_xy hl, 20, 80
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xf0

    @ld_xy hl, 70, 80
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xf0

    @ld_xy hl, 2, 1
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xf0
    ld a, 0x0f

    @ld_xy bc, 70, 8
    @ld_xy de, 70, 7
    @ld_xy hl, 70, 8
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xf0
    ld a, 0x0f

    @ld_xy bc, 20, 0
    @ld_xy de, 20, 199
    @ld_xy hl, 20, 0
    ld [hl], 0xff
    call Screen_PreviousLine
    ld [hl], 0xf0
    ld a, 0x0f

    @Screen_WaitVSync

    ret

draw_byte:
    @Debug_Border 02
    @ld_xy hl, 10, 10
    ld bc, 0x0208
    ld a, 0x3a
    call Draw_Byte

    ret

draw_byte_with_gap:
    @Debug_Border 02
    @ld_xy hl, 10, 25
    ld de, 0x0007
    ld bc, 0x0910
    ld a, 0x3a
    call Draw_ByteWithGap

    ret

draw_image:
    @Debug_Border 02
    ld hl, imageTest
    @ld_xy de, 20, 50
    ld bc, 0x0208
    call Draw_Image

    @Debug_Border 03
    ld hl, imageTest
    @ld_xy de, 20, 60
    ld bc, 0x0204
    call Draw_Image

    ret

apply_mask_and_draw_image:
    @Debug_Border 01
    @ld_xy hl, 10, 70
    ld bc, 0x080f
    ld a, 0xf0
    call Draw_Byte

    @Debug_Border 02
    ld hl, imageMaskTest
    @ld_xy de, 12, 72
    ld bc, 0x040d
    call Draw_ImageWithMask

    ret

draw_mask_in_image:
    @Debug_Border 01
    ld hl, imageTest
    @ld_xy de, 20, 90
    ld bc, 0x0208
    call Draw_Image
    @Debug_Border 02
    ld hl, maskTest1
    @ld_xy de, 20, 90
    ld bc, 0x0208
    call Draw_Mask

    @Debug_Border 01
    ld hl, imageTest
    @ld_xy de, 23, 90
    ld bc, 0x0208
    call Draw_Image
    @Debug_Border 02
    ld hl, maskTest2
    @ld_xy de, 23, 90
    ld bc, 0x0208
    call Draw_Mask

    @Debug_Border 01
    ld hl, imageTest
    @ld_xy de, 26, 90
    ld bc, 0x0208
    call Draw_Image
    @Debug_Border 02
    ld hl, maskTest3
    @ld_xy de, 26, 90
    ld bc, 0x0208
    call Draw_Mask

    @Debug_Border 01
    ld hl, imageTest
    @ld_xy de, 29, 90
    ld bc, 0x0208
    call Draw_Image
    @Debug_Border 02
    ld hl, maskTest4
    @ld_xy de, 29, 90
    ld bc, 0x0208
    call Draw_Mask

    ret

draw_and_copy_image:
    @Debug_Border 02
    ld hl, imageTest
    @ld_xy de, 10, 100
    ld bc, 0x0208
    call Draw_Image    ; draw one image

    @Debug_Border 03
    @ld_xy hl, 10, 100  ; from
    @ld_xy de, 10, 108  ; to
    ld bc, 0x0230     ; 2 bytes X 48 lines
    call Draw_CopyScreen   ; repeat vertical

    @Debug_Border 08
    @ld_xy hl, 10, 100  ; from
    @ld_xy de, 12, 100  ; to
    ld bc, 0x2028     ; 20 bytes X 40 lines
    call Draw_CopyScreen

    ret

draw_pattern:
    @Debug_Border 03
    ld hl, imageTest
    @ld_xy de, 10, 160
    ld bc, 0x0208
    ld a, 30
    call Draw_Pattern

    ret

draw_pattern_vs_copy_screen:
    @Debug_Border 02
    ld hl, imageTestDuo
    @ld_xy de, 0, 80
    ld bc, 0x0408
    call Draw_Image
    ;;
    @Debug_Border 03
    ld hl, imageTestDuo
    @ld_xy de, 0, 92
    ld bc, 0x0408
    ld a, 19
    call Draw_Pattern
    ;;
    @Debug_Border 07
    ld hl, imageTestDuo
    @ld_xy de, 0, 108
    ld bc, 0x0408
    call Draw_Image

    @ld_xy hl, 0, 108
    @ld_xy de, 4, 108
    ld bc, 0x4c08
    call Draw_CopyScreen

    ret

draw_byte_vs_copy_screen:
    @Debug_Border 02
    @ld_xy hl, 7, 100
    ld bc, 0x2010
    ld a, 0x3a
    call Draw_Byte
    ;;
    @Debug_Border 07
    @ld_xy hl, 7, 130
    ld [hl], 0xa3
    @ld_xy de, 7, 131
    ld bc, 0x010f
    call Draw_CopyScreen
    @ld_xy hl, 7, 130
    @ld_xy de, 8, 130
    ld bc, 0x1f10
    call Draw_CopyScreen
    ret

;;;;;;;;;;;;;;
;; Data
imageTest:
    DB 0x13, 0x10
    DB 0x30, 0xD6
    DB 0x73, 0xB6
    DB 0x30, 0xC0
    DB 0x01, 0x08
    DB 0x33, 0xEE
    DB 0x76, 0x5A
    DB 0x23, 0xA5
imageTestDuo:
    DB 0x13, 0x10, 0x14, 0x00
    DB 0x30, 0xD6, 0x52, 0x00
    DB 0x73, 0xB6, 0xF6, 0x00
    DB 0x30, 0xC0, 0x60, 0x00
    DB 0x01, 0x08, 0x02, 0x00
    DB 0x33, 0xEE, 0x77, 0xCC
    DB 0x76, 0x5A, 0xED, 0x84
    DB 0x23, 0xA5, 0x56, 0x48
maskTest1:
    DB 0xff, 0xff
    DB 0xaa, 0xaa
    DB 0xdd, 0xdd
    DB 0xaa, 0xaa
    DB 0xff, 0xff
    DB 0xaa, 0xaa
    DB 0xdd, 0xdd
    DB 0xaa, 0xaa
maskTest2:
    DB 0x55, 0x55
    DB 0xaa, 0xaa
    DB 0x55, 0x55
    DB 0xaa, 0xaa
    DB 0x55, 0x55
    DB 0xaa, 0xaa
    DB 0x55, 0x55
    DB 0xaa, 0xaa
maskTest3:
    DB 0x55, 0x11
    DB 0x88, 0xaa
    DB 0x55, 0x11
    DB 0x88, 0xaa
    DB 0x55, 0x11
    DB 0x88, 0xaa
    DB 0x55, 0x11
    DB 0x88, 0xaa
maskTest4:
    DB 0x00, 0x00
    DB 0x88, 0x22
    DB 0x00, 0x00
    DB 0x00, 0x00
    DB 0x11, 0x00
    DB 0x00, 0x00
    DB 0x00, 0x00
    DB 0x88, 0x22
imageMaskTest:
    DB 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00 ; Mask, Pixels, Mask, Pixels, ...
    DB 0xff, 0x00, 0x88, 0x00, 0x00, 0x00, 0x77, 0x00
    DB 0xff, 0x00, 0x88, 0x13, 0x00, 0x10, 0x33, 0x00
    DB 0xff, 0x00, 0x00, 0x30, 0x00, 0xD6, 0x33, 0x00
    DB 0xff, 0x00, 0x00, 0x73, 0x00, 0xB6, 0x33, 0x00
    DB 0xff, 0x00, 0x00, 0x30, 0x00, 0xC0, 0x77, 0x00
    DB 0xff, 0x00, 0x88, 0x00, 0x11, 0x00, 0xff, 0x00
    DB 0xff, 0x00, 0x00, 0x01, 0x00, 0x08, 0x77, 0x00
    DB 0xff, 0x00, 0x00, 0x33, 0x00, 0xEE, 0x33, 0x00
    DB 0xff, 0x00, 0x00, 0x76, 0x00, 0x5A, 0x33, 0x00
    DB 0xff, 0x00, 0x00, 0x23, 0x00, 0xA5, 0x33, 0x00
    DB 0xff, 0x00, 0x88, 0x00, 0x00, 0x00, 0x77, 0x00
    DB 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00
