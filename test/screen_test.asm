INCLUDE "test.h.asm"
;;
; INCLUDE "screen.h.asm"   ; already included in test.h.asm
@Test_Begin
;;
; INCLUDE "screen.asm"    ; already included in test.h.asm
;;
Test_SetUpBeforeCase:
    call Test_ClearScreen
    ret

@Test_Exec calculate_screen_address_from_horizontal_coordenates
    @Screen_Address 0, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC000, hl 
    @Screen_Address 1, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC001, hl 
    @Screen_Address 10, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC00A, hl 
    @Screen_Address 20, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC014, hl 
    @Screen_Address 39, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC027, hl 
    @Screen_Address 40, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC028, hl 
    @Screen_Address 79, 0
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC04F, hl 
    @Screen_Address 79, 199
    ld hl, .Screen_Address
    @Test_AssertEquals 0xFFCF, hl

@Test_Exec calculate_screen_address_from_vertical_coordenates
    @Screen_Address 0, 1
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC800, hl 
    @Screen_Address 0, 2
    ld hl, .Screen_Address
    @Test_AssertEquals 0xD000, hl
    @Screen_Address 0, 3
    ld hl, .Screen_Address
    @Test_AssertEquals 0xD800, hl
    @Screen_Address 0, 4
    ld hl, .Screen_Address
    @Test_AssertEquals 0xE000, hl
    @Screen_Address 0, 5
    ld hl, .Screen_Address
    @Test_AssertEquals 0xE800, hl
    @Screen_Address 0, 6
    ld hl, .Screen_Address
    @Test_AssertEquals 0xF000, hl
    @Screen_Address 0, 7
    ld hl, .Screen_Address
    @Test_AssertEquals 0xF800, hl
    @Screen_Address 0, 8
    ld hl, .Screen_Address
    @Test_AssertEquals 0xC050, hl
    @Screen_Address 0, 99
    ld hl, .Screen_Address
    @Test_AssertEquals 0xDBC0, hl
    @Screen_Address 0, 100
    ld hl, .Screen_Address
    @Test_AssertEquals 0xE3C0, hl
    @Screen_Address 0, 199
    ld hl, .Screen_Address
    @Test_AssertEquals 0xFF80, hl
    @Screen_Address 79, 199
    ld hl, .Screen_Address
    @Test_AssertEquals 0xFFCF, hl

@Test_Exec calculate_next_line
    @ld_xy hl, 0, 0
    call Screen_NextLine
    @Screen_Address 0, 1
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 0, 1
    call Screen_NextLine
    @Screen_Address 0, 2
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 0, 7
    call Screen_NextLine
    @Screen_Address 0, 8
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 17, 100
    call Screen_NextLine
    @Screen_Address 17, 101
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 73, 198
    call Screen_NextLine
    @Screen_Address 73, 199
    @Test_AssertEquals .Screen_Address, hl

@Test_Exec calculate_previous_line
    @ld_xy hl, 0, 1
    call Screen_PreviousLine
    @Screen_Address 0, 0
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 0, 2
    call Screen_PreviousLine
    @Screen_Address 0, 1
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 0, 8
    call Screen_PreviousLine
    @Screen_Address 0, 7
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 17, 100
    call Screen_PreviousLine
    @Screen_Address 17, 99
    @Test_AssertEquals .Screen_Address, hl

    @ld_xy hl, 73, 199
    call Screen_PreviousLine
    @Screen_Address 73, 198
    @Test_AssertEquals .Screen_Address, hl

@Test_Exec calculate_next_frame
    ld a, 6                 ;  Given speed  
    ld b, 12                ;  Given current frame

    call Screen_CalculateNextFrameTime   ;  When calculate next frame 

    @Test_AssertEquals 18, b            ;  Then next frame is the sum of speed and current frame

@Test_Exec calculate_next_frame_is_between_screen_frequency
    ld a, 8                 ;  Given speed  
    ld b, 45                ;  Given current frame
    
    call Screen_CalculateNextFrameTime   ;  When calculate next frame 

    @Test_AssertEquals 3, b              ; Then next frame is over screen frequency and must be reset to begin from 0

@Test_End