INCLUDE "test.h.asm"
;
INCLUDE "entity.h.asm"
INCLUDE "graphic.h.asm"
INCLUDE "animation.h.asm"
;
@Test_Begin
;;
INCLUDE "animation.asm"
;;
Test_SetUpBeforeCase:
    call Test_ClearScreen
    ret

@Test_Exec next_frame_in_time
    ld ix, Entity_$test_Entity
    ld [ix + ANIMATION_SPEED], 19
    ld hl, Screen_$frequency
    ld [hl], 37

    @Entity_NextFrameTime ANIMATION

    ld b, [ix + ANIMATION_FRAME]
    @Test_AssertEquals 6, b

@Test_Exec sprite_update_call_when_entity_sprite_frame_is_equal_to_timer
    ld ix, Entity_$test_Entity
    ld [ix + ANIMATION_SPEED], 9
    ld hl, Screen_$frequency
    ld [hl], 23

    @Entity_NextFrameTime ANIMATION

    ld b, [ix + ANIMATION_FRAME]
    @Test_AssertEquals 32, b

@Test_Exec update_sprite_when_frame_is_equal_to_timer
    ld ix, Entity_$test_Entity
    ld [ix + ANIMATION_SPEED], 10
    ld [ix + ANIMATION_FRAME], 25
    ld hl, Screen_$frequency
    ld [hl], 25

    call Animation_UpdateSprite

    ld b, [ix + ANIMATION_FRAME]    ; next sprite frame is the last
    @Test_AssertEquals ANIMATION_END_FRAME, b
    @Test_AssertEquals Sprite_$test_Sprite, iy ; IY has entity sprite address
    @Test_AssertEquals Animation_Null, hl  ; HL has function animation address

@Test_End

;; Data
@Sprite_Create Sprite_$test, 0x00, 0x00, 0x00, void
    DB null         ;; An empty sprite structure

@Entity_CreateAnimatedMoving Entity_$test, 0x00, 0x00, Sprite_$test, void, 0x00, 0x00, 0x00, 0x00, Animation_Null
