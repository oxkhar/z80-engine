
INCLUDE "common.asm"
INCLUDE "screen.asm"

;;
; Almacena un valor para comparación de registros
Test_$valueCompare: DW 0x0000
; Almacena contador de iteraciones bucles
Test_$loopCounter: DB 0x00

;;
; Compara el valor esperado con el valor en la dirección especificada
;
; Registros:
;   BC => Valor esperado
;   DE => Dirección con el valor a comparar
;   HL => Dirección de la pantalla para mostrar el resultado
;
; Modificados:  A, BC, DE, HL
Test_AssertWordEquals:
    ld a, [de]  ; Carga el valor en la dirección DE en A
    cp c        ; Compara A con el byte alto del valor esperado (C)
    jp nz, Test_AssertionFailed  ; Salta a Test_Error si no son iguales
    inc de      ; Incrementa DE para apuntar al siguiente byte
;   B  => Valor esperado
Test_AssertByteEquals:
    ld a, [de]  ; Carga el siguiente byte en A
    cp b        ; Compara A con el byte bajo del valor esperado (B)
    jp nz, Test_AssertionFailed  ; Salta a Test_Error si no son iguales

    jp z, Test_AssertionOk  ; Si son iguales, salta a Test_Ok
;;
;;
; Maneja el caso en que la aserción es correcta
; Registros:
;   HL => Dirección de la pantalla para mostrar el resultado
;
; Modificados: A, BC
Test_AssertionOk:
    ld a, 0x0F  ; Código de color para indicar éxito
    ld b, 2     ; Prepara el ancho para el dibujo
    ld c, 8     ; Prepara la altura para el dibujo
    call Test_DrawByte  ; Dibuja el byte en la pantalla
    ret
;;
; Maneja el caso en que la aserción falla
; Parámetros:
;   HL => Dirección de la pantalla para mostrar el resultado
;
; Modificados: A, BC
Test_AssertionFailed:
    ld a, 0xFF  ; Código de color para indicar error
    ld b, 2     ; Prepara el ancho para el dibujo
    ld c, 8     ; Prepara la altura para el dibujo
    call Test_DrawByte  ; Dibuja el byte en la pantalla
    ret

;;
; Limpia la pantalla
;
; Modificados: BC, DE, HL
Test_ClearScreen:
    ld bc, 0x4000 - 1  ; Establece el tamaño del área a limpiar
    ld hl, 0xC000      ; Dirección de inicio en la memoria
    ld de, 0xC000 + 1  ; Dirección de escritura
    ld [hl], 0x00      ; Escribe un byte cero
    ldir               ; Copia la memoria de HL a DE
    ret
;;
; Dibuja un byte en la pantalla en un área definida
;
; Parámetros:
;   HL => Dirección de la pantalla
;   B => Repeticiones (ancho)
;   C => Líneas (altura)
;   A => Byte a escribir
;
; Modificados: A, A', BC, HL
Test_DrawByte:
    push bc          ; Guarda BC en la pila
    push hl          ; Guarda HL en la pila
_loop:
    ld [hl], a       ; Escribe el byte en la dirección HL
    inc hl           ; Incrementa HL para el siguiente byte
    djnz _loop       ; Decrementa B y repite si B no es cero

    ex af, af'       ; Intercambia A con A'
    pop hl           ; Restaura HL
    pop bc           ; Restaura BC
    dec c            ; Decrementa el contador de líneas
    ret z            ; Retorna si C es cero

    @Screen_NextLineInHL  ; Calcula la siguiente línea en HL
    ex af, af'       ; Intercambia A con A'

    jp Test_DrawByte ; Salta de nuevo para dibujar más líneas
;;
; Dibuja reglas en el borde de la pantalla para comparar los dibujos en pantalla
;
; Modificados: A, HL, BC
Test_DrawBorderRules:
    ld a, 0xFF          ; Carga el color para el borde
    ld [0xD000], a      ; Dibuja en la parte superior
    ld [0xD000 + 0x800], a  ; Dibuja en la parte inferior

    ld a, 0x0F          ; Carga el color para las reglas laterales
    ld [0xD000 + 0x800 * 2], a  ; Dibuja en el lado izquierdo
    ld [0xD000 + 0x800 * 3], a  ; Dibuja en el lado derecho

    ld a, 0xF0          ; Carga otro color para el borde
    ld [0xD000 + 0x800 * 4], a  ; Dibuja más borde
    ld [0xD000 + 0x800 * 5], a  ; Dibuja más borde

    ld hl, 0xD000       ; Establece HL para la copia
    ld de, 0xC050       ; Establece DE para la escritura
    ld bc, 0x01BC       ; Establece el ancho de copia
    call _copyScreen    ; Llama a la rutina de copia de pantalla

    ld a, 0xFF          ; Prepara más color para otro borde
    ld [0xD04F], a      ; Dibuja en la parte superior
    ld [0xD04F + 0x800], a  ; Dibuja en la parte inferior

    ld a, 0x0F          ; Prepara otro color para las reglas laterales
    ld [0xD04F + 0x800 * 2], a  ; Dibuja en el lado izquierdo
    ld [0xD04F + 0x800 * 3], a  ; Dibuja en el lado derecho

    ld a, 0xF0                  ; Prepara más color para el borde
    ld [0xD04F + 0x800 * 4], a  ; Dibuja más borde
    ld [0xD04F + 0x800 * 5], a  ; Dibuja más borde

    ld hl, 0xD04F       ; Establece HL para la copia
    ld de, 0xC09F       ; Establece DE para la escritura
    ld bc, 0x01BC       ; Establece el ancho de copia
    call _copyScreen    ; Llama a la rutina de copia de pantalla

    ret
;;
; Copia una sección de la pantalla de una dirección a otra
;
; Parámetros:
;   HL => Dirección de la pantalla a leer
;   DE => Dirección de la pantalla a escribir
;   B => Ancho (en bytes)
;   C => Altura (en líneas)
;
; Modificados: A, BC, DE, HL, AF'
_copyScreen:
    ld a, c          ; Carga la altura en A
    ex af, af'       ; Intercambia A con A' (para guardar ancho)
    ld a, b          ; Carga el ancho en A
_loop:
    ld b, 0x00       ; Inicializa B para el ciclo de copia
    ld c, a          ; Carga el ancho en C
    ldir             ; Copia de HL a DE

    ex af, af'       ; Intercambia A (altura)
    dec a            ; Decrementa la altura
    ret z            ; Retorna si se han copiado todas las líneas

    ex af, af'       ; Intercambia A (ancho)
    neg              ; Negar A para ajustar el cálculo
    dec b            ; Decrementa B
    ld c, a          ; Ajusta C con la nueva altura

    add hl, bc           ; Ajusta HL para la próxima línea
    @Screen_NextLineInHL ; Calcula la siguiente línea en HL
    ex de, hl            ; Intercambia DE y HL para la copia
    add hl, bc           ; Ajusta HL para la siguiente copia
    @Screen_NextLineInHL ; Calcula la siguiente línea en HL
    ex de, hl            ; Restaura DE

    ld a, c          ; Carga la altura nuevamente
    neg              ; Negar A para control de bucle
    jr _loop         ; Vuelve al inicio del bucle
