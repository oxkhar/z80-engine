INCLUDE "test.h.asm"
;
INCLUDE "graphic.h.asm"
INCLUDE "entity.h.asm"
;
@Test_Begin
;
INCLUDE "draw.asm"
INCLUDE "entity.asm"
;
Test_SetUpBeforeCase:
    call Test_ClearScreen
    ret

@Test_Exec draw_entity
    ld ix, Juanito_$Entity

    call Entity_Draw

@Test_End

;;
; Data...
@Sprite_Create Foto, 1, 3, 4, Draw_Image
    DB 0xff, 0x0f, 0xf0
    DB 0xf0, 0x0f, 0xff
    DB 0x0f, 0xff, 0xf0
    DB 0xff, 0xf0, 0x0f

@Entity_Create Juanito, 10, 50, Foto, void

