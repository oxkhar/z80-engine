INCLUDE "common.h.asm"
INCLUDE "screen.h.asm"

;;
; Activar depuración para ver los bordes de colores para medir
; la ejecución de los procesos
DEBUG DEFL true

;;
; Comienzo para iniciar una suite de pruebas
.Test_Begin DEFL Test_Default
@Test_Begin MACRO .testName
  IF ! NUL .testName
    .Test_Begin DEFL Test_ ## .testName
  ENDIF

    ORG 0x4000

Test_Begin:
    call Common_DisableFirmware
    jp Test_Init

  INCLUDE "test_case.asm"
ENDM

;;
; Finalizar la suite de pruebas para que quede parada la ejecución
@Test_End MACRO
Test_End:
    jr Test_End
ENDM

@Test_Repeat MACRO
    jp Test_Begin
ENDM

;;
; Inicializar la suite de pruebas
@Test_SetUpCase MACRO .testName
  IF ! DEFINED Test_Init
Test_Init:
    IF DEFINED Test_SetUpBeforeCase
    call Test_SetUpBeforeCase
    ENDIF
    IF ! DEFINED .Test_Begin
      .ERROR .Test_Begin not defined
    ENDIF
    jp .Test_Begin
Test_Default:
  ENDIF
Test_ ## .testName:
  IF DEFINED Test_SetUp_ ## .testName
    call Test_SetUp_ ## .testName
  ELSE 
    IF DEFINED Test_SetUp
    call Test_SetUp
    ENDIF
  ENDIF
ENDM

@Test_TearDownCase MACRO .testName
Test_End_ ## .testName:
  IF DEFINED Test_TearDown_ ## .testName
    call Test_TearDown_ ## .testName
  ELSE 
    IF DEFINED Test_TearDown
    call Test_TearDown
    ENDIF
  ENDIF
ENDM

;;
; Macro para ejecutar un bucle de prueba
; Parámetros:
;   .testName: nombre de la prueba (opcional, en cuyo caso el nombre del test es .fnPreVSync)
;   .fnPreVSync: función a llamar antes de VSync
;   .fnPostVSync: función a llamar después de VSync (opcional)
@Test_Loop MACRO .testName, .fnPreVSync, .fnPostVSync
    @Test_SetUpCase .testName
    @Debug_Border 04

_loop:
    call .fnPreVSync
    @Debug_Border 04

    @Screen_WaitVSync
    halt
    halt

    call .fnPostVSync
    @Debug_Border 04

    jp _loop
 
    @Test_TearDownCase .testName

ENDM

;;
; 
; Parámetros:
;   .testName: nombre de la prueba 
@Test_Wait MACRO .testName
    @Test_SetUpCase .testName
    @Debug_Border 04

    ld hl, Test_$loopCounter
    ld [hl], 0xFF ; 255 iteraciones

_loop:
    call .testName
    @Debug_Border 04

    @Screen_WaitVSync
    halt
    halt

    ld hl, Test_$loopCounter
    dec [hl]
    jp nz, _loop
 
    @Test_TearDownCase .testName

ENDM

;;
; Variables de posición en pantalla para imprimir el resultado de las
; aserciones en los test...
; Azul correcto, Rojo incorrecto
.Test_PosX DEFL 0
.Test_PosY DEFL 0

;;
; Macro para definir la ejecución de un test
; Parámetros:
;   .testName: nombre de la prueba a ejecutar
@Test_Exec MACRO .testName
    .Test_PosX DEFL 10
    .Test_PosY DEFL (.Test_PosY + 12)

    @Test_SetUpCase .testName
ENDM

;;
; Macros de aserción para validar resultados
;;

; Initialize registers
IRP .reg, .AssertRegBC, .AssertRegDE, .AssertRegHL, .AssertRegIX, .AssertRegIY
    .reg DEFL false
ENDM
;;
; Aserción para comparar registros
; Parámetros:
;   .expected: valor esperado
;   .register: registro a comparar A,B,C,D,E,H,L,BC,DE,HL,IX,IY
@Test_AssertEquals MACRO .expected, .register
    .AssertReg ## .register DEFL true

IF .AssertRegBC || .AssertRegDE || .AssertRegHL || .AssertRegIX || .AssertRegIY
    @Test_AssertWordEquals .expected, .register
ELSE
    @Test_AssertByteEquals .expected, .register
ENDIF

    .AssertReg ## .register DEFL false
ENDM

;;
; Aserción para comparar un registro con un valor esperado
; Parámetros:
;   .expected: valor esperado
;   .register: registro a comparar A,B,C,D,E,H,L
@Test_AssertByteEquals MACRO .expected, .register
    ld a, .register
    ld [Test_$valueCompare], a

    @Test_AssertAddressContains .expected, Test_$valueCompare
ENDM

;;
; Aserción para comparar un par de registros
; Parámetros:
;   .expected: valor esperado
;   .pair: par de registros a comparar BC,DE,HL,IX,IY
@Test_AssertWordEquals MACRO .expected, .pair
    ld [Test_$valueCompare], .pair

    @Test_AssertAddressContainsWord .expected, Test_$valueCompare
ENDM

;;
; Aserción para comparar un valor con el valor almacenado en una dirección
; Parámetros:
;   .value: valor esperado (WORD)
;   .address: dirección con el valor a comparar
@Test_AssertAddressContains MACRO .value, .address
    exx
    @ld_xy hl, .Test_PosX, .Test_PosY
    ld de, .address
    ld b, .value
    call Test_AssertByteEquals
    exx

    .Test_PosX DEFL (.Test_PosX + 3)
ENDM

;;
; Aserción para comparar un valor con el valor almacenado en una dirección
; Parámetros:
;   .value: valor esperado (WORD)
;   .address: dirección con el valor a comparar
@Test_AssertAddressContainsWord MACRO .value, .address
    exx
    ld bc, .value
    ld de, .address
    @ld_xy hl, .Test_PosX, .Test_PosY
    call Test_AssertWordEquals
    exx

    .Test_PosX DEFL (.Test_PosX + 3)
ENDM
