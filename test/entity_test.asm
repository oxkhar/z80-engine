INCLUDE "test.h.asm"
;
INCLUDE "graphic.h.asm"
INCLUDE "entity.h.asm"
;
@Test_Begin
;
INCLUDE "entity.asm"
;
Test_SetUpBeforeCase:
    call Test_ClearScreen
    ret

@Test_Exec entity_create
    ld ix, Pepito_$Entity
    ld a, [ix + ENTITY_STATUS]
    @Test_AssertEquals ENTITY_STATUS_DIRTY, a
    ld a, [ix + ENTITY_POS_X]
    @Test_AssertEquals 0, a
    ld a, [ix + ENTITY_POS_Y] 
    @Test_AssertEquals 0, a
    ld h, [ix + ENTITY_SCREEN_H] 
    ld l, [ix + ENTITY_SCREEN_L] 
    @Screen_Address 0, 0
    @Test_AssertEquals .Screen_Address, hl
    ld h, [ix + ENTITY_IMAGE_H] 
    ld l, [ix + ENTITY_IMAGE_L] 
    @Test_AssertEquals null, hl
    ld h, [ix + ENTITY_SPRITE_H] 
    ld l, [ix + ENTITY_SPRITE_L]
    @Test_AssertEquals null, hl
    ld h, [ix + ENTITY_UPDATE_H]
    ld l, [ix + ENTITY_UPDATE_L]
    @Test_AssertEquals void, hl

@Test_Exec entity_create_with_images
    ld ix, Juanito_$Entity
    ld h, [ix + ENTITY_SPRITE_H] 
    ld l, [ix + ENTITY_SPRITE_L]
    @Test_AssertEquals Foto_$Sprite, hl
    ld h, [ix + ENTITY_IMAGE_H] 
    ld l, [ix + ENTITY_IMAGE_L] 
    @Test_AssertEquals Foto_$Data, hl
    
    ld ix, Jorgito_$Entity
    ld h, [ix + ENTITY_SPRITE_H] 
    ld l, [ix + ENTITY_SPRITE_L]
    @Test_AssertEquals Foto_$Sprite, hl
    ld h, [ix + ENTITY_IMAGE_H] 
    ld l, [ix + ENTITY_IMAGE_L] 
    @Test_AssertEquals null, hl

@Test_Exec entiy_set_positions
    ld ix, Pepito_$Entity
    ld [ix + ENTITY_STATUS], ENTITY_STATUS_OK

    @Entity_Position Pepito, 20, 100

    ld a, [ix + ENTITY_STATUS]
    @Test_AssertEquals ENTITY_STATUS_DIRTY, a
    ld a, [ix + ENTITY_POS_X]
    @Test_AssertEquals 20, a
    ld a, [ix + ENTITY_POS_Y] 
    @Test_AssertEquals 100, a
    ld h, [ix + ENTITY_SCREEN_H] 
    ld l, [ix + ENTITY_SCREEN_L] 
    @Screen_Address 20, 100
    @Test_AssertEquals .Screen_Address, hl

@Test_Exec entity_move_left
    @Entity_Position Pepito, 16, 152

    call Entity_MoveLeftOne

    ld d, [ix + ENTITY_SCREEN_H]
    ld e, [ix + ENTITY_SCREEN_L]
    @Screen_Address 15, 152
    @Test_AssertEquals .Screen_Address, DE  ; DE equals to screen address for x,y

    ld b, [ix + ENTITY_POS_X]
    ld c, [ix + ENTITY_POS_Y]
    @Test_AssertEquals 15, b
    @Test_AssertEquals 152, c

@Test_Exec entity_move_right
    @Entity_Position Pepito, 16, 152

    call Entity_MoveRightOne

    ld b, [ix + ENTITY_POS_X]
    ld c, [ix + ENTITY_POS_Y]
    @Test_AssertEquals 17, b
    @Test_AssertEquals 152, c

    ld d, [ix + ENTITY_SCREEN_H]
    ld e, [ix + ENTITY_SCREEN_L]
    @Screen_Address 17, 152
    @Test_AssertWordEquals .Screen_Address, DE  ; DE equals to screena address for x,y

@Test_End

;;
; Data...
@Sprite_Create Foto, 0, 0, 0, void 

@Entity_Create Pepito, 0, 0, null, void
@Entity_Create Juanito, 10, 50, Foto, void
@Entity_Create Jorgito, 0, 0, Foto_$Sprite, void

