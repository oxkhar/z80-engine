
;;
; Image structure field offsets
IMAGE_ADDR          EQU 0
IMAGE_ADDR_L        EQU 0
IMAGE_ADDR_H        EQU 1
IMAGE_WIDTH         EQU 2
IMAGE_HEIGHT        EQU 3
IMAGE_SIZE          EQU 4
IMAGE_DRAW_FUNC     EQU 5
IMAGE_DRAW_L        EQU 5
IMAGE_DRAW_H        EQU 6
IMAGE_STRUCT_SIZE   EQU 7

;;
; Creates an image structure
; Parameters:
;   .name = Image identifier
;   .image = Image data address
;   .width = Width in bytes
;   .height = Height in pixels
;   .draw = Draw function address
; Returns:
;   .name ## Image = Image structure
@Image_Create MACRO .name, .addrImage, .width, .height, .draw
.name ## _Image:
    DW .addrImage
    DB .width
    DB .height
    DB .width * .height   ; Total size in bytes
    DW .draw
ENDM

;;
; Sprite structure field offsets
SPRITE_NUM_IMAGES    EQU IMAGE_STRUCT_SIZE + 0
SPRITE_BG_ADDR       EQU IMAGE_STRUCT_SIZE + 1
SPRITE_BG_L          EQU IMAGE_STRUCT_SIZE + 1
SPRITE_BG_H          EQU IMAGE_STRUCT_SIZE + 2
SPRITE_BG_SIZE       EQU IMAGE_STRUCT_SIZE + 3
SPRITE_STRUCT_SIZE   EQU IMAGE_STRUCT_SIZE + 4

;;
; Creates a sprite structure
; Parameters:
;   .name = Sprite identifier where the images are stored
;   .numImages = Number of animation frames
;   .width = Width in bytes
;   .height = Height in pixels
;   .draw = Draw function address
@Sprite_Create MACRO .name, .numImages, .width, .height, .draw
.name ## _Sprite:

    @Image_Create .name, .name ## _Data, .width, .height, .draw

    DB .numImages
    DW .name ## _Background
.name ## _Background:
    DS .width * .height

.name ## _Data:
ENDM

@Sprite_Image MACRO .name, .numImage
  .name ## _Data ## .numImage:
ENDM
