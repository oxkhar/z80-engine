;;
; Erase Package Functions
;;

; INCLUDE "entity.h.asm"
; INCLUDE "graphic.h.asm"

; Create stack for cleaning entities
@Stack_Create stackCleaner, 32

;;
; Clean screen area where entity sprite was drawn
; Parameters:
;   IX = Entity address
; Modifies:
;   AF, HL
Erase_EntitySprite:
    ld a, [ix + MOTION_ERASE_H]
    or 0
    ret nz

    ld h, [ix + ENTITY_SCREEN_H]
    ld l, [ix + ENTITY_SCREEN_L]
    ld [ix + MOTION_ERASE_H], h
    ld [ix + MOTION_ERASE_L], l

    ld hl, stackCleaner
    jp Stack_AddItem

;;
; Clears all sprites from screen
; Modifies:
;   AF, BC, DE, HL, IX, IY
Erase_AllSprites:
    @Stack_Walker entityClearWalker, stackCleaner, Erase_$clearSprite, true

    ld hl, stackCleaner
    jp Stack_Empty

;;
; Internal function to clear a single sprite
; Parameters:
;   IX = Entity address
;   IY = Sprite data
; Modifies:
;   AF, BC, DE, HL
Erase_$clearSprite:
    ld h, [ix + MOTION_ERASE_H]
    ld l, [ix + MOTION_ERASE_L]
    ld b, [iy + IMAGE_WIDTH]
    ld c, [iy + IMAGE_HEIGHT]
    xor a
    ld [ix + MOTION_ERASE_H], 0x00
    ld [ix + MOTION_ERASE_L], 0x00

    jp Draw_Byte  ; TODO: Should restore background instead of clearing
