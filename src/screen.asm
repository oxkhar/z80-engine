;;
; Screen management functions
; Contains vertical sync and color management utilities

PUBLIC Screen_$frequency

Screen_$frequency:   DB 0x00         ; Screen frequency counter

; Hardware ports and commands
PPI_PORT_B           EQU 0xF5        ; PPI port B input address
GATE_ARRAY_PORT      EQU 0x7F        ; Gate Array port address
INKR_COMMAND         EQU 0x40        ; INKR command base value (CCCnnnnn)

;;
; Calculates the next frame time based on speed and current frame
; Parameters:
;   A = Source speed value
;   B = Current frame time
; Returns:
;   B <- Next frame when action must be applied
; Modifies:
;   A, F
Screen_CalculateNextFrameTime:
    add a, b                             ; Calculate next frame draw time
    ld b, a                             ; Store result in B

    sub SCREEN_FREQUENCY                 ; Subtract screen frequency to check if it's time for the next frame
    ret m                              ; Return if the result is negative (i.e., not time for next frame)

    ld b, a                             ; If time for next frame, reset B to the valid interval
    ret

;;
; Waits for vertical sync signal from PPI
; Modifies:
;   A, F, BC
Screen_WaitVSync:
    ld b, PPI_PORT_B                 
_waitLoop:
    in a, [c]                    ; Read PPI port B input
    rra                          ; Move bit 0 to carry flag
    jp nc, _waitLoop             ; Loop if vsync not active
    ret

;;
; Waits specified time then syncs with vertical refresh
; Parameters:
;   B = Number of halts (1 halt ≈ 0.003 seconds)
; Modifies:
;   A, F, BC
Screen_WaitTime:
    halt
    djnz Screen_WaitTime
    jp Screen_WaitVSync

; Modify HL video address to next line
; HL => Current Address
; Destroy: A
; Returns: HL => New address
Screen_NextLine:
    @Screen_NextLineInHL
    ret

; Modify HL video address to previous line
; HL => Current Address
; Destroy: A
; Returns: HL => New address
Screen_PreviousLine:
    @Screen_PreviousLine
    ret

;;
; Changes color settings for specified pen
; Parameters:
;   H = Ink value to set
;   L = Pen number to modify
; Modifies:
;   A, B
Screen_ChangeColour:
    ld b, GATE_ARRAY_PORT           
    out [c], l                      ; Select pen number
    
    ld a, INKR_COMMAND              
    or h                            ; Combine with ink value
    out [c], a                      ; Set ink for selected pen
    ret

;;
; Clears entire screen
; This routine initializes the screen by filling it with zeros,
; effectively clearing the entire display area.
; Modifies:
;   AF, BC, DE, HL
Screen_Clear:
    ld bc, 0x4000 - 1        ; Set BC to the number of bytes to clear (16383 bytes)
    ld hl, SCREEN_ADDRESS     ; Load HL with the starting address of the screen
    ld de, SCREEN_ADDRESS + 1 ; Load DE with the next address to write (HL + 1)
    ld [hl], 0x00            ; Write 0 to the current screen address to clear it
    ldir                      ; Repeat the write operation BC times (fill the screen with 0s)
    ret                       ; Return from the routine
