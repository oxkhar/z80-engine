;;
; Stack Package Functions
;;

;;
; Resets a stack by clearing all items
; Parameters:
;   HL = Stack address pointer
; Modifies:
;   DE, HL
; Cycles: 27
Stack_Reset:
    inc hl                             ; Point to next position

    ld [hl], STACK_END                 ; Set end marker
    dec hl
    ld [hl], STACK_END

    ld d, h                           ; Save current address in DE
    ld e, l

    dec hl                           ; Update last item pointer
    ld [hl], d
    dec hl
    ld [hl], e

    ret

;;
; Adds item to stack. All stacks must have STACK_END at the end
; Parameters:
;   IX = Item address pointer
;   HL = Stack address pointer
; Modifies:
;   AF, HL, DE
; Cycles: 49
Stack_AddItem:
    dec hl                           ; Point to stack control structure
    dec hl
;;
; Main add item routine
; Parameters:
;   IX = Item address pointer
;   HL = End of stack memory pointer (StackNameLast)
; Modifies:
;   AF, HL, DE
; Cycles: 47
Stack_AddItemMain:
    ld e, [hl]                       ; Load address of pointer to use in DE
    inc hl
    ld d, [hl]                       ; Position to new item address in DE

    ld a, ixl                        ; Store IX into [DE]
    ld [de], a                       ; New item is added
    inc de                           
    ld a, ixh                        
    ld [de], a                       

    inc de
    ld [hl], d                       ; Save high address position from new item
    dec hl                           ; as end of stack
    ld [hl], e

    ex de, hl                        ; Now HL is new address of end of stack
    inc hl                           ; Append stack end values next to new item
    ld [hl], STACK_END               
    inc hl                           
    ld [hl], STACK_END               
    ret

;;
; Searches and removes an item from stack
; Parameters:
;   IX = Entity address pointer
;   HL = Stack address pointer
; Modifies:
;   AF, HL, DE
Stack_RemoveItem:
    ld d, h                          ; Now DE is pointing to stack
    ld e, l                          
    dec hl                           ; and HL will be pointer to last item
    dec hl
;;
; Main remove item routine
; Parameters:
;   IX = Entity address pointer
;   HL = Address with pointer to end of stack
;   DE = Stack address pointer
; Modifies:
;   AF, HL, DE
Stack_RemoveItemMain:
_nextItem:
    ld a, [de]                       ; Search entity in stack
    cp ixl                           ; Compare with IX low byte
    jp nz, _itemSlotLowNotFound      
    inc de                           
    ld a, [de]                       
    cp ixh                           ; Compare with IX high byte
    jp nz, _itemSlotHighNotFound     
_itemFound:
        push hl                           ; Save pointer to last item
        push de                           ; and current item

        ld e, [hl]                       ; Load address to last item from pointer
        inc hl
        ld d, [hl]
        pop hl                           ; Recover item address found in HL

        dec de
        ld a, [de]                       ; Remove item by overwriting with last item
        ld [hl], a                       
        ld a, STACK_END                  ; Mark old last item position as end
        ld [de], a                       
        dec hl
        dec de
        ld a, [de]                       ; Handle low address value
        ld [hl], a                       
        ld a, STACK_END                  
        ld [de], a                       

        pop hl                           ; Save new last item address into pointer
        ld [hl], e                       
        inc hl                           
        ld [hl], d                       

        ret
_itemSlotLowNotFound:
    inc de
_itemSlotHighNotFound:
    inc de
    jp _nextItem

;;
; Return the last item in a stack
;
; HL => Stack address
; IX <- Entity address
; Modifies:
;   A, HL, DE
Stack_LastItem:
    dec hl
    ld d, [hl]
    dec hl
    ld e, [hl] ; DE now point to last element
Stack_LastItemMain:
    dec de
    ld a, [de]
    ld ixh, a
    dec de
    ld a, [de]
    ld ixl, a
    ret

;;
; Iterate over a stack and call function for each item
;
; DE => Stack address
; HL => Function address
; Modifies:
;   IX, IY, DE, HL, A
Stack_Walker:
_loopNext:
    ld a, [de]
    ld ixl, a
    inc de
    ld a, [de]
    ld ixh, a
    inc de

    cp ixl             ; if IXL <> IXH is a valid address
    jr nz, _loopBegin  ; go to the body of loop
    cp STACK_END       ; else if are equals to 0xFF (enf of stack)
    jr z, _loopExit    ; exit of loop
    cp STACK_GAP       ; else if is a empty gap
    jr z, _loopNext    ; go to the next element
_loopBegin:            ; else begin body of loop
        push hl
        push de

        ld d, HIGH _loopReturn
        ld e, LOW  _loopReturn
        push de            ; Return address, not need POP

        jp [hl]            ; Calling function for each item address in IX
_loopReturn:
    pop de
    pop hl
    jp _loopNext
_loopExit:
    ret

if DEFINED ENTITY_SPRITE_L
;;
; Iterate over a stack and call function for each entity with graphics
;   DE = Stack address
;   HL = Function address
;   IX <- Current entity
;   IY <- Entity graphic (image/sprite)
; Modifies:
;   IX, IY, DE, HL, A
Stack_EntityWalker:
_loopNext:
    ld a, [de]
    ld ixl, a
    inc de
    ld a, [de]
    ld ixh, a
    inc de

    cp ixl             ; if IXL <> IXH is a valid address
    jr nz, _loopBegin  ; go to the body of loop
    cp STACK_END       ; else if are equals to 0xFF (end of stack)
    jr z, _loopExit    ; exit of loop
    cp STACK_GAP       ; else if is a empty gap
    jr z, _loopNext    ; go to the next element
_loopBegin:            ; else begin body of loop
        push hl
        push de

        ld d, HIGH _loopReturn
        ld e, LOW  _loopReturn
        push de            ; Return address, not need POP

        ld a, [ix + ENTITY_SPRITE_H] ;
        ld iyh, a                   ;
        ld a, [ix + ENTITY_SPRITE_L] ;
        ld iyl, a                   ; IY with sprite structure

        jp [hl]               ; Calling function for each entity in IX
                              ; and graphics into IY
_loopReturn:
    pop de
    pop hl
    jp Stack_EntityWalker
_loopExit:
    ret
ENDIF
