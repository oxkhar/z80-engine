;;
; This package provides functionality for drawing tile patterns and repeating patterns on the screen.
; It includes routines to handle the drawing of tiles at specified screen locations and to create
; repeating patterns based on given image data.
; The package manages memory addresses for reading and writing tile data, allowing for efficient
; rendering of graphics in a game or graphical application.

;; 
Draw_$readAddress:    DW null
Draw_$writeAddress:   DW null
Draw_$nextAddress:    DW null

;;
; TODO...
; Draws a tile pattern at specified screen location
; Parameters:
;   DE = Screen address to write
;   HL = Tile data address
;   B = Width in bytes
;   C = Height in lines
; Modifies:
;   AF, BC, DE, HL, AF'
Draw_Tile:
    ret

;;
; Draws a repeating pattern
; Parameters:
;   HL = Image data address
;   DE = Screen address
;   B = Width in bytes
;   C = Height in lines
;   A = Number of repeats
; Returns:
;   DE <- Next position after pattern
; Modifies:
;   AF, BC, DE, HL, AF', BC'
Draw_Pattern:
    push af                          ; Store the current value of AF (repeat count)
    ld [Draw_$readAddress], hl       ; Load the tile data address into the read address
    ld [Draw_$writeAddress], de      ; Load the screen address into the write address
    ld a, c                          ; Load height into register A
    ex af, af'                       ; Save height in AF' for later use
    ld a, b                          ; Load width into register A
    ld b, 0x00                       ; Set B to 0, preparing for bytes to copy
    ld c, a                          ; Set C to the width in bytes
    ldir                             ; Draw the first line of the pattern
    ld [Draw_$nextAddress], de       ; Store the next address after the first line is drawn
    jp Draw__PatternNextLoop         ; Jump to the next loop to continue drawing

Draw__PatternLoop:
    ldir                             ; Draw the next line of the pattern
Draw__PatternNextLoop:
    ld [Draw_$readAddress], hl       ; Update the read address for the next line

    ld hl, [Draw_$writeAddress]      ; Load the current write address for horizontal repeat
    exx                              ; Exchange registers to prepare for repeat
    pop bc                           ; Restore repeat count from the stack into BC
    push bc                          ; Push it back onto the stack for use in the loop
_drawImage:
    exx                              ; Exchange registers again to get back to original state
    ld c, a                          ; Load the width into C for the ldir operation
    ldir                             ; Draw the image data horizontally

    exx                              ; Exchange registers to prepare for repeat
    djnz _drawImage                  ; Decrement B (repeat count) and repeat if not zero

    ex af, af'                       ; Restore height from AF'
    dec a                            ; Decrement height (one less line to draw)
    jp z, Draw__PatternExit           ; If height is zero, exit the drawing routine

    ex af, af'                       ; Restore width from AF'
    ld c, a                          ; Set C to the width (B is already 0)

    ld hl, [Draw_$readAddress]       ; Load the read address again
    ld de, [Draw_$writeAddress]      ; Load the write address again
    calculateNextLineDE              ; Calculate the next screen line address
    ld [Draw_$writeAddress], de      ; Update the write address with the new line

    ld a, c                          ; Restore width into A for the next iteration

    jp Draw__PatternLoop             ; Jump back to the loop to continue drawing
Draw__PatternExit:
    pop af                           ; Restore the original value of AF
    ld de, [Draw_$nextAddress]       ; Load the next address into DE for return
    ret                              ; Return from the Draw_Pattern routine

;;
; Draws an image at specified location
; Parameters:
;   HL = Image data address
;   DE = Screen address
;   B = Width in bytes
;   C = Height in lines
; Modifies:
;   AF, BC, DE, HL, AF'
Draw_Image:
    ld a, c                          ; Load height (number of lines) into register A
    ex af, af'                       ; Save height in AF' for later use
    ld a, b                          ; Load width (number of bytes per line) into register A
_loop:
    ld b, 0x00                       ; Set B to 0, preparing for the number of bytes to copy
    ld c, a                          ; Set C to the width in bytes for the ldir operation
    ldir                             ; Copy the current line of image data from HL to DE (screen)

    ex af, af'                       ; Restore height from AF' to A
    dec a                            ; Decrement height (indicating one less line to draw)
    ret z                            ; If height is zero, exit the drawing routine

    ex af, af'                       ; Restore width from AF' to A

    dec b                            ; Decrement B (which is zero, so this will be 0xFF)
    neg                              ; Negate B to get -width for address calculation
    ld c, a                          ; Set C to the width (BC = -width for next line calculation)

    ex de, hl                        ; Exchange DE and HL to prepare for address adjustment
    add hl, bc                       ; Adjust HL to point to the start address minus width (move back one line)
    @Screen_NextLineInHL             ; Move to the next screen line (adjust HL for the next line)
    ex de, hl                        ; Restore DE and HL after adjustment

    ld a, c                          ; Load width back into A for the next iteration
    neg                              ; Negate A to ensure it is positive for the next loop

    jr _loop                         ; Jump back to the loop to continue drawing lines
    ret                              ; Return from the Draw_Image routine

;;
; Draws an image with transparency mask
; Parameters:
;   HL = Image data address (source of image data)
;   DE = Screen address (destination where the image will be drawn)
;   B = Width in bytes (number of bytes per line of the image)
;   C = Height in lines (number of lines to draw)
; Modifies:
;   AF, BC, DE, HL, AF (registers used during the operation)
Draw_ImageWithMask:
    ld a, b                          ; Load the width (number of bytes) into register A
    ex af, af'                       ; Save the width in AF' for later use
_loop:                               ; Start of the loop for each line
    push de                          ; Save the current screen address (DE) on the stack
_lineLoop:                           ; Start of the loop for each pixel in the line
    ld a, [de]                       ; Load the current pixel from the screen into A
    and [hl]                         ; Apply the mask to the current pixel (AND operation)
    inc hl                           ; Move to the next pixel in the image data
    or [hl]                          ; Combine the masked pixel with the image pixel (OR operation)
    ld [de], a                       ; Write the resulting pixel back to the screen
    inc de                           ; Move to the next pixel in the screen
    inc hl                           ; Move to the next pixel in the image data
    djnz _lineLoop                   ; Decrement B (number of pixels) and repeat if not zero

    pop de                           ; Restore the screen address from the stack
    dec c                            ; Decrement C (one less line to draw)
    ret z                            ; If all lines are drawn, exit the routine

    @Screen_NextLineInDE             ; Move to the next screen line (adjust DE for the next line)

    ex af, af'                       ; Restore the width from AF'
    ld b, a                          ; Load the width back into B for the next iteration
    ex af, af'                       ; Restore the original value of AF

    jr _loop                         ; Jump back to the start of the mask loop for the next line
    ret                              ; Return from the Draw_ImageWithMask routine

;;
; Applies a mask to screen area
; Parameters:
;   HL = Mask data address
;   DE = Screen address
;   B = Width in bytes
;   C = Height in lines
; Modifies:
;   AF, BC, DE, HL, AF
Draw_Mask:
    ld a, b                          ; Load the width (number of bytes) into register A
    ex af, af'                       ; Save the width in AF' for later use
_loop:                               ; Start of the loop for each line
    push de                          ; Save the current screen address (DE) on the stack
_lineLoop:                           ; Start of the loop for each pixel in the line
    ld a, [de]                       ; Load the current pixel from the screen into A
    and [hl]                         ; Apply the mask to the current pixel (AND operation)
    ld [de], a                       ; Write the masked pixel back to the screen
    inc de                           ; Move to the next pixel in the screen
    inc hl                           ; Move to the next pixel in the mask data
    djnz _lineLoop                   ; Decrement B (number of pixels) and repeat if not zero

    pop de                           ; Restore the screen address from the stack
    dec c                            ; Decrement C (one less line to draw)
    ret z                            ; If all lines are drawn, exit the routine

    @Screen_NextLineInDE             ; Move to the next screen line (adjust DE for the next line)

    ex af, af'                       ; Restore the width from AF'
    ld b, a                          ; Load the width back into B for the next iteration
    ex af, af'                       ; Restore the original value of AF

    jr _loop                         ; Jump back to the start of the mask loop for the next line
    ret                              ; Return from the Draw_Mask routine

;;
; Copies screen area to another location
; Parameters:
;   HL = Source screen address
;   DE = Destination screen address
;   B = Width in bytes
;   C = Height in lines
; Modifies:
;   AF, BC, DE, HL, AF'
Draw_CopyScreen:
    ld a, c                          ; Load height (number of lines) into register A
    ex af, af'                       ; Save height in AF' for later use
    ld a, b                          ; Load width (number of bytes) into register A
_loop:
    ld b, 0x00                       ; Clear B to use it as a counter for the LDIR instruction
    ld c, a                          ; Set C to the current height (number of lines)
    ldir                             ; Copy the screen area from HL to DE for the width specified

    ex af, af'                       ; Restore A to hold the height again
    dec a                            ; Decrement A (one less line to copy)
    ret z                            ; If all lines are copied, exit the routine

    ex af, af'                       ; Restore A to hold the width again

    neg                              ; Negate A to prepare for the next calculations
    dec b                            ; Decrement B to adjust for the next line's address
    ld c, a                          ; Load the width back into C for the next iteration

    add hl, bc                       ; Adjust HL to point to the next line in the source
    @Screen_NextLineInHL             ; Move to the next line in the source screen address

    ex de, hl                        ; Swap DE and HL to prepare for the next copy
    add hl, bc                       ; Adjust HL to point to the next line in the destination
    @Screen_NextLineInHL             ; Move to the next line in the destination screen address
    ex de, hl                        ; Restore DE to point to the destination

    ld a, c                          ; Load the current height back into A
    neg                              ; Negate A to prepare for the loop condition

    jr _loop                         ; Jump back to the start of the copy loop for the next line
    ret                              ; Return from the Draw_CopyScreen routine

;;
; Draws repeated byte with gap between each
; Parameters:
;   HL = Screen address (where to write the byte)
;   DE = Gap size in bytes (space between each byte written)
;   B = Number of repeats (how many times to write the byte)
;   C = Number of lines (how many lines to write the byte with gaps)
;   A = Byte to write (the actual byte value to be written)
; Modifies:
;   AF, BC, DE, HL (registers used during the operation)
Draw_ByteWithGap:
    push hl               ; Save the current HL register (screen address)
    push bc               ; Save the current BC register (repeat count and gap size)
_loop:
    ld [hl], a            ; Write the byte (A) to the current screen address (HL)
    add hl, de            ; Move HL forward by the gap size (DE)
    djnz _loop            ; Decrement B and repeat if not zero

    ex af, af'            ; Restore the original value of AF (height)
    pop bc                ; Restore the BC register
    dec c                 ; Decrement C (number of lines left to write)
    ret z                 ; If C is zero, return from the routine

    pop hl                ; Restore the HL register (screen address)
    @Screen_NextLineInHL  ; Move to the next line in the screen address
    ex af, af'            ; Restore the original value of AF

    jp Draw_ByteWithGap   ; Jump back to the start to repeat for the next line

;;
; Draws repeated byte
; Parameters:
;   HL = Screen address (where to write the byte)
;   B = Number of repeats (width) (how many times to write the byte in a row)
;   C = Number of lines (height) (how many lines to write the byte)
;   A = Byte to write (the actual byte value to be written)
; Modifies:
;   AF, BC, DE, HL (registers used during the operation)
Draw_Byte:
    push bc               ; Save the current BC register (repeat count and line count)
    push hl               ; Save the current HL register (screen address)
_loop:
    ld [hl], a            ; Write the byte (A) to the current screen address (HL)
    inc hl                ; Move HL to the next position in the screen
    djnz _loop            ; Decrement B and repeat the loop if B is not zero

    ex af, af'            ; Restore the original value of AF (height)
    pop hl                ; Restore the HL register (screen address)
    pop bc                ; Restore the BC register (repeat count and line count)
    dec c                 ; Decrement C (number of lines left to write)
    ret z                 ; If C is zero, return from the routine

    @Screen_NextLineInHL  ; Move to the next line in the screen address
    ex af, af'            ; Restore the original value of AF (height)

    jp Draw_Byte          ; Jump back to the start to repeat for the next line

