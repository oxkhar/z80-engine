;;
; Initializes animation for an entity sprite
; Parameters:
;   IX = Entity structure pointer
; Returns:
;   IY <- Sprite entity pointer
; Modifies:
;   AF, BC, IY
Animation_InitializeSprite:
    ld a, [ix + ENTITY_SPRITE_H]      ; Load high byte of graphic pointer
    ld iyh, a                          ; Store in IY high byte
    ld a, [ix + ENTITY_SPRITE_L]      ; Load low byte of graphic pointer
    ld iyl, a                          ; Load sprite structure into IY

Animation_InitializeWithGraphicLoaded:
    @Entity_NextFrameTime ANIMATION           ; Set up the next frame timing

    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]  ; Mark entity status as dirty
    ld [ix + ANIMATION_IMAGE], 0x00   ; Initialize animation image number
    ld [ix + ANIMATION_STATE], 0x00     ; Initialize animation status
    ld [ix + ANIMATION_LOOPS], 0x00      ; Initialize animation loops

    ld h, [iy + IMAGE_ADDR_H]          ; Set initial image address high byte
    ld l, [iy + IMAGE_ADDR_L]          ; Set initial image address low byte
    ld [ix + ENTITY_IMAGE_H], h        ; Store high byte of image address
    ld [ix + ENTITY_IMAGE_L], l        ; Store low byte of image address

    ret

;;
; Updates animation state for an entity sprite
; Parameters:
;   IX = Entity structure pointer
; Returns:
;   IY <- Updated sprite entity pointer
; Modifies:
;   AF, HL, IY
Animation_UpdateSprite:
    ld a, [Screen_$frequency]           ; Load current timer value
    cp [ix + ANIMATION_FRAME]            ; Compare with current animation frame
    ret nz                              ; Return if not time to update sprite

    @Entity_NextFrameTime ANIMATION           ; Calculate next frame time

    ld a, [ix + ENTITY_SPRITE_H]      ; Load sprite structure high byte
    ld iyh, a                          ; Store in IY high byte
    ld a, [ix + ENTITY_SPRITE_L]      ; Load sprite structure low byte
    ld iyl, a                          ; Store in IY low byte

    ld h, [ix + ANIMATION_ANIMATE_H]      ; Get animation function high byte
    ld l, [ix + ANIMATION_ANIMATE_L]      ; Get animation function low byte

    jp [hl]                             ; Call animation function

;;
; No animation - always draws current image
; Parameters:
;   IX = Entity structure
; Modifies:
;   None
Animation_Null:
    ld [ix + ANIMATION_FRAME], ANIMATION_END_FRAME
    ret

;;
; Plays animation sequence once
; Parameters:
;   IX = Entity structure
;   IY = Sprite structure
; Modifies:
;   AF, BC, HL
Animation_SpriteOnce:
    inc [ix + ANIMATION_IMAGE]  ; Increment current image number

    ld a, [iy + SPRITE_NUM_IMAGES]; Load total number of images in sprite
    cp [ix + ANIMATION_IMAGE]   ; Compare with current image number
    jr z, _spriteOnceEnd          ; Jump to end if last image reached
        set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS] ; Mark entity status as dirty
        ld b, 0
        ld c, [iy + IMAGE_SIZE]    ; Load size of each image
        ld h, [ix + ENTITY_IMAGE_H] ; Load high byte of current image address
        ld l, [ix + ENTITY_IMAGE_L] ; Load low byte of current image address
        add hl, bc                ; Move to next image address
        jr _spriteOnceReturn
_spriteOnceEnd:
    ld [ix + ANIMATION_FRAME], ANIMATION_END_FRAME ; Set animation frame to end
    ld [ix + ANIMATION_IMAGE], 0x00   ; Reset image number
    inc [ix + ANIMATION_LOOPS]           ; Increment loop count
    ld h, [iy + IMAGE_ADDR_H]          ; Load high byte of first image address
    ld l, [iy + IMAGE_ADDR_L]          ; Load low byte of first image address
_spriteOnceReturn:
    ld [ix + ENTITY_IMAGE_H], h        ; Store high byte of image address
    ld [ix + ENTITY_IMAGE_L], l        ; Store low byte of image address
    ret

;;
; Loops animation sequence continuously
; Parameters:
;   IX = Entity structure
;   IY = Sprite structure
; Modifies:
;   AF, BC, HL
Animation_SpriteLoop:
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS] ; Mark entity status as dirty

    inc [ix + ANIMATION_IMAGE]  ; Increment current image number

    ld a, [iy + SPRITE_NUM_IMAGES]; Load total number of images in sprite
    cp [ix + ANIMATION_IMAGE]   ; Compare with current image number
    jr z, _spriteLoopReset        ; Jump to reset if last image reached
        ld b, 0
        ld c, [iy + IMAGE_SIZE]    ; Load size of each image
        ld h, [ix + ENTITY_IMAGE_H] ; Load high byte of current image address
        ld l, [ix + ENTITY_IMAGE_L] ; Load low byte of current image address
        add hl, bc                ; Move to next image address
        jr _spriteLoopReturn
_spriteLoopReset:
    inc [ix + ANIMATION_LOOPS]     ; Increment loop count
    ld [ix + ANIMATION_IMAGE], 0x00 ; Reset image number
    ld h, [iy + IMAGE_ADDR_H]    ; Load high byte of first image address
    ld l, [iy + IMAGE_ADDR_L]    ; Load low byte of first image address
_spriteLoopReturn:
    ld [ix + ENTITY_IMAGE_H], h  ; Store high byte of image address
    ld [ix + ENTITY_IMAGE_L], l  ; Store low byte of image address
    ret

;;
; Bounces animation sequence once
; Parameters:
;   IX = Entity structure
;   IY = Sprite structure
; Returns:
;   A <- Animation completion status
; Modifies:
;   AF, BC, HL
Animation_SpriteBounceOnce:
    ld hl, _spriteBounceOnce                    ; Load address of bounce once code
    ld [Animation_$animateSpriteBounceCode], hl ; Store it for later use
    jp Animation_SpriteBounceLoop              ; Jump to bounce loop
_spriteBounceOnce:
    res ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE]   ; Reset bounce step
    ld [ix + ANIMATION_FRAME], ANIMATION_END_FRAME      ; Set animation frame to end
    ld [ix + ANIMATION_IMAGE], 0x00                     ; Reset image number
    inc [ix + ANIMATION_LOOPS]                          ; Increment loop count
    ret

;;
; Bounces animation sequence loop
; Parameters:
;   IX = Entity structure
;   IY = Sprite structure
; Returns:
;   A <- Animation completion status
; Modifies:
;   AF, BC, HL
Animation_SpriteBounceAlways:
    ld hl, _spriteBounceAlways                          ; Load address of bounce loop code
    ld [Animation_$animateSpriteBounceCode], hl         ; Store it for later use
    jp Animation_SpriteBounceLoop                       ; Jump to bounce loop
_spriteBounceAlways:
    res ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE]   ; Reset bounce step
    add a, 0x02                                         ; Adjust animation status
    inc [ix + ANIMATION_LOOPS]                          ; Increment loop count
    jp Animation__animateSpriteBounceForward            ; Jump to forward bounce

;;
; Bounces animation sequence generic main loop from bounce loop
; This is the main loop that is used to bounce the animation sequence
; It is used for all bounce animations, infinite and once
;
; Parameters:
;   IX = Entity structure
;   IY = Sprite structure
; Returns:
;   A <- Animation completion status
; Modifies:
;   AF, BC, HL
Animation_SpriteBounceLoop:
    ld a, [ix + ANIMATION_IMAGE]                        ; Load current image number
    bit ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE]   ; Check bounce step
    jp z, Animation__animateSpriteBounceNext            ; Jump if not bouncing backward
        dec a                                           ; Decrement image number
        jp p, Animation__animateSpriteBounceBackward    ; Jump if positive
;   JP 0xXXXX
    DB 0xC3                         ; Placeholder for JP instruction
Animation_$animateSpriteBounceCode:
    DW 0x0000                       ; Placeholder for address to jump when bouncing is finished

Animation__animateSpriteBounceNext:
    inc a                       ; Increment image number
    cp [iy + SPRITE_NUM_IMAGES] ; Compare with total images
    jp nz, Animation__animateSpriteBounceForward ; Jump if not last image
        set ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE] ; Set bounce step
        sub 0x02                ; Adjust animation status
        jp Animation__animateSpriteBounceBackward ; Jump to backward bounce
Animation__animateSpriteBounceForward:
    ld [ix + ANIMATION_IMAGE], a             ; Store current image number
    ld b, 0x00
    ld c, [iy + IMAGE_SIZE]                  ; Load size of each image
    jp Animation__animateSpriteBounceImage   ; Jump to image calculation
Animation__animateSpriteBounceBackward:
        ld [ix + ANIMATION_IMAGE], a    ; Store current image number
        ld a, [iy + IMAGE_SIZE]         ; Load size of each image
        neg                        ; Negate to move backward
        ld b, 0xFF                 ; Set B for subtraction
        ld c, a                    ; Store negated size in C
Animation__animateSpriteBounceImage:
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS] ; Mark entity status as dirty
    ld h, [ix + ENTITY_IMAGE_H]  ; Load high byte of current image address
    ld l, [ix + ENTITY_IMAGE_L]  ; Load low byte of current image address
    add hl, bc                    ; Adjust image address
Animation__animateSpriteBounceReturn:
    ld [ix + ENTITY_IMAGE_H], h  ; Store high byte of image address
    ld [ix + ENTITY_IMAGE_L], l  ; Store low byte of image address
    ret

;;
; Legacy bounce animation implementation
; Parameters:
;   IX = Entity structure
;   IY = Sprite structure
; Modifies:
;   AF, BC, HL
Animation_SpriteBounceLegacy:
    bit ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE] ; Check bounce step
    jr z, _bounceForward        ; Jump if not bouncing backward
_bounceBackward:
    dec [ix + ANIMATION_IMAGE] ; Decrement image number
    jp m, _bounceFinish         ; Jump if negative (end of bounce)
        set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS] ; Mark entity status as dirty
        ld a, [iy + IMAGE_SIZE]  ; Load size of each image
        neg                     ; Negate to move backward
        ld b, 0xff              ; Set B for subtraction
        ld c, a                 ; Store negated size in C
        jr _bounceImage         ; Jump to image calculation
_bounceFinish:
    res ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE] ; Reset bounce step
    ld [ix + ANIMATION_FRAME], ANIMATION_END_FRAME ; Set animation frame to end
    ld [ix + ANIMATION_IMAGE], 0x00   ; Reset image number
    ret

_bounceForward:
    ld a, [ix + ANIMATION_IMAGE] ; Load current image number
    inc a                          ; Increment image number
    cp [iy + SPRITE_NUM_IMAGES]    ; Compare with total images
    jr nz, _bounceContinue         ; Jump if not last image
        set ANIMATION_BOUNCE_STEP, [ix + ANIMATION_STATE] ; Set bounce step
        jr _bounceBackward         ; Jump to backward bounce
_bounceContinue:
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS] ; Mark entity status as dirty
    ld [ix + ANIMATION_IMAGE], a ; Store current image number
    ld b, 0x00
    ld c, [iy + IMAGE_SIZE]         ; Load size of each image
_bounceImage:
    ld l, [ix + ENTITY_IMAGE_L]   ; Load low byte of current image address
    ld h, [ix + ENTITY_IMAGE_H]   ; Load high byte of current image address
    add hl, bc                     ; Adjust image address
_bounceReturn:
    ld [ix + ENTITY_IMAGE_L], l   ; Store low byte of image address
    ld [ix + ENTITY_IMAGE_H], h   ; Store high byte of image address
    ret
