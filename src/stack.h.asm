
PUBLIC STACK_END, STACK_GAP

STACK_HEADER  EQU 0xFFFF     ; Marks the end of a stack

STACK_END    EQU 0xFFFF     ; Marks the end of a stack
STACK_GAP    EQU 0x0000     ; Represents an empty slot in the stack

;;
; Creates a new stack structure with specified capacity
; Parameters:
;   .name      = Stack identifier
;   .numItems  = Maximum number of items
;   .fillWith  = Optional: Initial value (default: STACK_END)
; Creates:
;   Stack_${.name}Pointer  = Current position pointer
;   Stack_${.name}Last     = Points to stack start
;   .name                 = Actual stack array
@Stack_Create MACRO .name, .numItems, .fillWith
    LOCAL .Stack_fillValue
    .Stack_fillValue DEFL STACK_END
    IF ! NUL .fillWith
        .Stack_fillValue DEFL .fillWith
    ENDIF

    ; Stack control structure
.name ## _CurrentPointer:
    DW 0x0000                    ; Current position pointer
.name ## _EndStack:
    DW .name                     ; Points to stack start
.name:
    DS .numItems * 2, .Stack_fillValue
    DW STACK_END                 ; Stack terminator
ENDM

;;
; Adds an item to the stack
; Parameters:
;   IX = Entity address pointer
;   .name = Stack identifier
; Modifies:
;   AF, HL
@Stack_AddItem MACRO .name
    ld hl, .name ## _EndStack
    call Stack_AddItemMain
ENDM

;;
; Removes an item from the stack
; Parameters:
;   IX = Entity address pointer
;   .name = Stack identifier
; Modifies:
;   AF, DE, HL
@Stack_RemoveItem MACRO .name
    ld de, .name
    ld hl, .name ## _EndStack
    call Stack_RemoveItemMain
ENDM

;;
; Gets the last item from the stack
; Parameters:
;   .name = Stack identifier
; Returns:
;   HL <- Last item value
; Modifies:
;   AF, DE, HL
@Stack_LastItem MACRO .name
    ld hl, .name ## _EndStack
    ld e, [hl]
    inc hl
    ld d, [hl]
    call Stack_LastItemMain
ENDM

;;
; Clear all items from the stack
; Parameters:
;   .name = Stack identifier
; Modifies:
;   DE, HL
@Stack_Reset MACRO .name
    ld hl, .name
    call Stack_Reset
ENDM

;;
; Base iterator macro for stack operations. Need a macro for each loop
; We must define an address for exit of loop with the name...
;     Stack__loop ## .nameLoop ## Exit
; And need to define the end of loop invoking ...
;     Stack__loop ## .nameLoop ## Next
; So we can decide how to end each loop
; Parameters:
;   .nameLoop     = Unique loop identifier
;   .name         = Stack identifier
;   .fnAddress    = Function to call for each item
;   .withGraphics = If true, loads graphics data into IY
; Modifies:
;   AF, BC, DE, HL, IX, IY (if graphics)
@Stack__LooperMain MACRO .nameLoop, .name, .fnAddress, .withGraphics
    ld de, .name
Stack__loop ## .nameLoop ## Next:
    ; Load item address into IX
    ld a, [de]
    ld ixl, a
    inc de
    ld a, [de]
    ld ixh, a
    inc de

    cp ixl                                         ; If IXL <> IXH, it is a valid address
    jr nz, Stack__loop ## .nameLoop ## Begin       ; Go to the body of loop
    cp STACK_END                                   ; Else if equal to 0xFF (end of stack)
    jr z, Stack__loop ## .nameLoop ## Exit         ; Exit loop when end
    cp STACK_GAP                                   ; Else if it is an empty gap
    jr z, Stack__loop ## .nameLoop ## Next         ; Go to the next element
Stack__loop ## .nameLoop ## Begin:
        push de

    IF ! NUL .withGraphics
        ld a, [ix + entityHexGraphicH]
        ld iyh, a
        ld a, [ix + entityHexGraphicL]
        ld iyl, a
    ENDIF

        ld hl, Stack__loop ## .nameLoop ## Return
        push hl
        jp .fnAddress

Stack__loop ## .nameLoop ## Return:
    pop de
ENDM

;;
; Iterates over stack items and calls function for each
; Parameters:
;   .nameLoop     = Unique loop identifier
;   .name         = Stack identifier
;   .fnAddress    = Function to call for each item
;   .withGraphics = If true, loads graphics data into IY
; Modifies:
;   AF, BC, DE, HL, IX, IY (if graphics)
@Stack_Walker MACRO .nameLoop, .name, .fnAddress, .withGraphics
    @Stack__LooperMain .nameLoop, .name, .fnAddress, .withGraphics
    jp Stack__loop ## .nameLoop ## Next
Stack__loop ## .nameLoop ## Exit:
ENDM

;;
; Searches for items in the stack
; Parameters:
;   .nameLoop     = Unique loop identifier
;   .name         = Stack identifier
;   .fnAddress    = Function returning true/false in A if item found
;   .withGraphics = If true, loads graphics data into IY
; Returns:
;   A <- true if item found, false otherwise
; Modifies:
;   AF, BC, DE, HL, IX, IY (if graphics)
@Stack_Finder MACRO .nameLoop, .name, .fnAddress, .withGraphics
    @Stack__LooperMain .nameLoop, .name, .fnAddress, .withGraphics
    or false
    jp z, Stack__loop ## .nameLoop ## Next
    jp Stack__loop ## .nameLoop ## Found
Stack__loop ## .nameLoop ## Exit:
    ld a, false
Stack__loop ## .nameLoop ## Found:
ENDM

;;
; [!] TODO: This is not working as expected. Review it
;
; Removes current item during stack iteration
; Parameters:
;   .name = Stack identifier
; Modifies:
;   AF, BC, DE, HL
@Stack_RemoveCurrentItem MACRO .name
    pop bc                     ; Return address for current process
    pop de                     ; Next item address
    dec de                     ; Point to HIGH byte of current item

    ld h, d
    ld l, e
    dec hl                    ; Point to current element
    push hl                   ; Save as next element
    push bc                   ; Restore return address

    ld hl, Stack_$ ## .name ## Last
    call Stack_ItemSlotFound
ENDM

