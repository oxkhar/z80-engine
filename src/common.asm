;;
; Empty function that just returns
void:
    ret

;;
; Address of the RST 38h interrupt handler
RST_INT_ADDRESS EQU 0x0038

;;
; Disables firmware interrupt handling and sets up a minimal interrupt handler
; that just enables interrupts and returns
; Modifies:
;   HL
Common_DisableFirmware:
    di                      ; Disable interrupts
    im 1                    ; Set interrupt mode 1 (RST 38h)

    ld hl, 0xc9fb          ; Load EI (0xfb) and RET (0xc9) opcodes
    ld [RST_INT_ADDRESS], hl ; Install minimal interrupt handler

    ei                      ; Enable interrupts
    ret
