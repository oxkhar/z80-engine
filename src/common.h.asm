;;
; Common constants and debug macros
; Contains basic boolean values and debugging utilities

PUBLIC true, false, null

; Boolean constants
true    EQU 0xFF
false   EQU 0x00
null    EQU 0x0000     ; For address/pointer initialization

;;;; DEBUG UTILITIES ;;;;

DEBUG DEFL false

;;
; Changes border color for visual debugging
; Parameters:
;   .color:     Number of color to set
;   .numHalts:  Number of HALT cycles to wait (optional)
; Modifies:
;   HL
@Debug_Border MACRO .color, .numHalts
    IF ! DEBUG
        EXITM
    ENDIF

    ld hl, 0x0010 + .color * 0x0100
    call Screen_ChangeColour

    IF ! NUL .numHalts
        REPT .numHalts
            halt
        ENDM
    ENDIF
ENDM

;;
; Experimental counter interrupt routine
; TODO: Rename variables following naming convention
; TODO: Add proper documentation
@Debug_CounterInt MACRO
    xor a
    ld [contador], a

    ld a, [contador]
    cp 0
    jr z, _counterEnd
    ld b, a
    ld hl, 0xc008
_counterLoop:
    ld [hl], 0x3c
    inc hl
    djnz _counterLoop
_counterEnd:
    jr _counterEnd
ENDM
