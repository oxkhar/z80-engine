;;
; Updates the entity using its update function
; IX => Entity (points to the entity data)
Entity_Update:
    ld l, [ix + ENTITY_UPDATE_L]    ; Load the low byte of the update function address from the entity structure
    ld h, [ix + ENTITY_UPDATE_H]    ; Load the high byte of the update function address from the entity structure
    jp [hl]                          ; Jump to the update function pointed to by HL

;;
; Initializes motion for an entity
; This function sets up the motion state for the entity.
; Parameters:
;   IX = Entity address (points to the entity data structure)
; Modifies:
;   AF, HL (registers used during the operation)
Entity_InitializeMotion:
    @Entity_NextFrameTime MOTION                    ; Update the screen with the next frame time for motion
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]   ; Mark the entity's status as dirty (indicating it needs to be updated)
    ret                                             ; Return from the function

;;
; Moves the entity one position to the right
; Parameters:
;   IX = Entity address
; Modifies:
;   AF, HL
Entity_MoveRightOne:
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]
    inc [ix + ENTITY_POS_X]
    inc [ix + ENTITY_SCREEN_ADDR]   ; move to right screen address
    ret nz
    inc [ix + ENTITY_SCREEN_H]      ; add one to HIGH if LOW loop
    ret

;;
; Moves the entity one position to the left
; Parameters:
;   IX = Entity address
; Modifies:
;   AF, HL
Entity_MoveLeftOne:
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]
    xor a
    dec [ix + ENTITY_POS_X]
    cp [ix + ENTITY_SCREEN_ADDR]    ; move to left screen address
    jp nz, _moveLeftOneOver
        dec [ix + ENTITY_SCREEN_H]
_moveLeftOneOver:
    dec [ix + ENTITY_SCREEN_L]
    ret

;;
; Moves the entity to the right by a specified number of bytes
; Parameters:
;   IX = Entity address
;   C = Number of bytes to move
; Modifies:
;   AF, HL
Entity_MoveRight:
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]
    ld a, [ix + ENTITY_POS_X]
    add a, c
    ld [ix + ENTITY_POS_X], a
    ld a, [ix + ENTITY_SCREEN_L]   ; move to right screen address
    add a, c
    ld [ix + ENTITY_SCREEN_L], a
    ret nc
    inc [ix + ENTITY_SCREEN_H]
    ret

;;
; Initializes graphics for an entity
; Parameters:
;   IX = Entity address
; Modifies:
;   AF, HL, IY
Entity_InitializeGraphics:
    ld a, [ix + ENTITY_SPRITE_H]
    ld iyh, a
    ld a, [ix + ENTITY_SPRITE_L]
    ld iyl, a

    ld h, [iy + IMAGE_ADDR_H]
    ld l, [iy + IMAGE_ADDR_L]
    ld [ix + ENTITY_IMAGE_H], h
    ld [ix + ENTITY_IMAGE_L], l

    @Entity_NextFrameTime ANIMATION
    set ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]
    ret

;;
; Draws an entity sprite if its dirty flag is set
; Parameters:
;   IX = Entity address
; Modifies:
;   AF, BC, DE, HL, IY
Entity_DrawIfDirty:
    bit ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]
    ret z
    res ENTITY_STATUS_DIRTY, [ix + ENTITY_STATUS]
    jp Entity_Draw

;;
; Draws an entity sprite at its current position
; Parameters:
;   IX = Entity address
; Returns:
;   IY = Sprite graphics data
; Modifies:
;   AF, BC, DE, HL, IY
Entity_Draw:
    ld a, [ix + ENTITY_SPRITE_H]
    ld iyh, a
    ld a, [ix + ENTITY_SPRITE_L]
    ld iyl, a

    ld b, [iy + IMAGE_WIDTH]
    ld c, [iy + IMAGE_HEIGHT]

    ld d, [iy + IMAGE_DRAW_H]
    ld e, [iy + IMAGE_DRAW_L]

    ld iyh, d
    ld iyl, e

    ld d, [ix + ENTITY_SCREEN_H]
    ld e, [ix + ENTITY_SCREEN_L]
    ld h, [ix + ENTITY_IMAGE_H]
    ld l, [ix + ENTITY_IMAGE_L]

    jp [iy]
