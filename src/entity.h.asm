;;
; When creating an entity, the entity structure is created with the following identifiers
;   .name ## _Entity        
;   .name ## _Position   .name ## _PositionX   .name ## _PositionY
;   .name ## _Animation  
;   .name ## _Motion          
; For example, if the entity is named "Player", the following identifiers are created:
;   Player_Entity
;   Player_Position     Player_PositionX     Player_PositionY
;   Player_Animation    
;   Player_Motion

;;
; Entity status constants
ENTITY_STATUS_OK        EQU 0x00
ENTITY_STATUS_DIRTY     EQU 0x01
ENTITY_STATUS_FINISHED  EQU 0x02
ENTITY_STATUS_DESTROYED EQU 0x04

;;
; Entity structure field offsets
ENTITY_STATUS        EQU 0  ; Status value
ENTITY_POS           EQU 1  ; Position fields
ENTITY_POS_X         EQU 1  ; X position in screen (bytes, Mode 1: 1 byte = 4px)
ENTITY_POS_Y         EQU 2  ; Y position in pixels
ENTITY_SCREEN_ADDR   EQU 3  ; Screen address for drawing
ENTITY_SCREEN_L      EQU 3  ; Screen address low byte
ENTITY_SCREEN_H      EQU 4  ; Screen address high byte
ENTITY_SPRITE_ADDR   EQU 5  ; Sprite data address
ENTITY_SPRITE_L      EQU 5  ; Sprite address low byte
ENTITY_SPRITE_H      EQU 6  ; Sprite address high byte
ENTITY_IMAGE_ADDR    EQU 7  ; Image data address
ENTITY_IMAGE_L       EQU 7  ; Image address low byte
ENTITY_IMAGE_H       EQU 8  ; Image address high byte
ENTITY_UPDATE_FUNC   EQU 9  ; Update function pointer
ENTITY_UPDATE_L      EQU 9  ; Update function low byte
ENTITY_UPDATE_H      EQU 10 ; Update function high byte
ENTITY_STRUCT_SIZE   EQU 11

;;
; Creates a new entity structure
; Parameters:
;   .name = Entity identifier
;   .x = Initial X position
;   .y = Initial Y position
;   .sprite = Sprite data address
;   .fnUpdate = Update function address
; Returns:
;   .name ## Entity = Entity structure
@Entity_Create MACRO .name, .x, .y, .sprite, .fnUpdate
  LOCAL .entitySprite
  LOCAL .entityImage
  IF DEFINED .sprite ## _Sprite
    .entitySprite DEFL .sprite ## _Sprite
    .entityImage  DEFL .sprite ## _Data
  ELSE 
    .entitySprite DEFL .sprite
    .entityImage DEFL null    ; Provisional, no puedo hallar directamente la imagen
  ENDIF

  @Screen_Address .x, .y

.name ## _Entity:
    DB ENTITY_STATUS_DIRTY   ; Entity status
    DB .x                    ; X position (bytes, max 79)
    DB .y                    ; Y position (lines, max 199)
    DW .Screen_Address       ; Draw address (calculated)
    DW .entitySprite         ; Sprite data address
    DW .entityImage          ; Current image data address

    DW .fnUpdate              ; Update function

ENDM

;;
; Defines screen position constants
; Parameters:
;   .name = Position identifier
;   .x = X coordinate in bytes (0-79)
;   .y = Y coordinate in pixels (0-199)
; Returns:
;   .name ## Position  = Screen address
;   .name ## PositionX = X coordinate
;   .name ## PositionY = Y coordinate
@Entity_Position MACRO .name, .x, .y
    @Screen_Address .x, .y
    .name ## _Position  DEFL .Screen_Address
    .name ## _PositionX DEFL .x
    .name ## _PositionY DEFL .y

    ld ix, .name ## _Entity

    ld [ix + ENTITY_STATUS], ENTITY_STATUS_DIRTY
    ld [ix + ENTITY_POS_X], .name ## _PositionX
    ld [ix + ENTITY_POS_Y], .name ## _PositionY
    ld [ix + ENTITY_SCREEN_H], HIGH .name ## _Position 
    ld [ix + ENTITY_SCREEN_L], LOW .name ## _Position

ENDM

;;
; Animation structure field offsets
ANIMATION_SPEED         EQU ENTITY_STRUCT_SIZE + 0
ANIMATION_FRAME         EQU ENTITY_STRUCT_SIZE + 1
ANIMATION_IMAGE         EQU ENTITY_STRUCT_SIZE + 2
ANIMATION_STATE         EQU ENTITY_STRUCT_SIZE + 3
ANIMATION_LOOPS         EQU ENTITY_STRUCT_SIZE + 4
ANIMATION_ANIMATE_FUNC  EQU ENTITY_STRUCT_SIZE + 5
ANIMATION_ANIMATE_L     EQU ENTITY_STRUCT_SIZE + 5
ANIMATION_ANIMATE_H     EQU ENTITY_STRUCT_SIZE + 6
ANIMATION_STRUCT_SIZE   EQU ENTITY_STRUCT_SIZE + 7

;;
; Creates an animation structure
; Parameters:
;   .name = Animation identifier
;   .speed = Animation speed (1-50)
;   .fnAnimation = Animation function address
; Returns:
;   .name ## Animation = Animation structure
@Entity_Animation MACRO .name, .speed, .fnAnimation
.name ## _Animation:
    DB .speed               ; Animation speed (1-50)
    DB ANIMATION_END_FRAME  ; Next frame time
    DB 0x00                 ; Current image index
    DB null                 ; Animation state
    DB null                 ; Loop counter
    DW .fnAnimation         ; Animation function address
ENDM

;;
; Motion structure field offsets
MOTION_SPEED        EQU ANIMATION_STRUCT_SIZE + 0
MOTION_FRAME        EQU ANIMATION_STRUCT_SIZE + 1
MOTION_STEP_X       EQU ANIMATION_STRUCT_SIZE + 2
MOTION_STEP_Y       EQU ANIMATION_STRUCT_SIZE + 3
MOTION_ERASE_ADDR   EQU ANIMATION_STRUCT_SIZE + 4
MOTION_ERASE_L      EQU ANIMATION_STRUCT_SIZE + 4
MOTION_ERASE_H      EQU ANIMATION_STRUCT_SIZE + 5
MOTION_STRUCT_SIZE  EQU ANIMATION_STRUCT_SIZE + 6

;;
; Creates a motion structure
; Parameters:
;   .name = Motion identifier
;   .stepX = X movement step size
;   .stepY = Y movement step size
;   .speed = Movement speed (1-50)
; Returns:
;   .name ## Motion = Motion structure
@Entity_Motion MACRO .name, .stepX, .stepY, .speed
.name ## _Motion:
    DB .speed               ; Movement speed (1-50)
    DB ANIMATION_END_FRAME  ; Next movement time
    DB .stepX               ; X step size (bytes)
    DB .stepY               ; Y step size (pixels)
    DW null                 ; Previous draw address
ENDM

;;
; Creates an entity with animation and movement
; Parameters:
;   .name = Entity identifier
;   .x = Initial X position
;   .y = Initial Y position
;   .sprite = Sprite data address
;   .fnUpdate = Update function address
;   .stepX = X movement step size
;   .stepY = Y movement step size
;   .motionSpeed = Movement speed
;   .animSpeed = Animation speed
;   .fnAnimation = Animation function address
@Entity_CreateAnimatedMoving MACRO .name, .x, .y, .sprite, .fnUpdate, .stepX, .stepY, .motionSpeed, .animSpeed, .fnAnimation
    @Entity_Create .name, .x, .y, .sprite, .fnUpdate
    @Entity_Animation .name, .animSpeed, .fnAnimation
    @Entity_Motion .name, .stepX, .stepY, .motionSpeed
ENDM

;;
; Creates an entity with only animation
; Parameters:
;   .name = Entity identifier
;   .x = Initial X position
;   .y = Initial Y position
;   .animSpeed = Animation speed
;   .sprite = Sprite data address
;   .fnUpdate = Update function address
;   .fnAnimation = Animation function address
@Entity_CreateAnimated MACRO .name, .x, .y, .sprite, .fnUpdate, .animSpeed, .fnAnimation
    @Entity_Create .name, .x, .y, .sprite, .fnUpdate
    @Entity_Animation .name, .animSpeed, .fnAnimation
ENDM

;;
; Calculates next frame time for motion or animation action and updates the frame
; Parameters:
;   .actionType = Field to update ("MOTION" or "ANIMATION")
;   IX = Entity structure pointer
; Modifies:
;   A
@Entity_NextFrameTime MACRO .actionType
    ld a, [Screen_$frequency]                ; With current screen frequency
    add a, [ix + .actionType ## _SPEED]      ;  add speed to set which frmae 
    ld [ix + .actionType ## _FRAME], a       ;  is the next time to apply action
    sub SCREEN_FREQUENCY                     ; Subtract screen frequency to calculate
    jp m, _skipFrequencyReset                ;  if next frecuency is over     
        ld [ix + .actionType ## _FRAME], a   ;  and reset counter at begin
_skipFrequencyReset:
ENDM

