;;
INCLUDE "test_case.asm"

;;
testEntity MACRO _test

    INCLUDE "entity.h.asm"
    INCLUDE "stack.h.asm"
    INCLUDE "animate.h.asm"

    testExec _test

ENDM

;;
testCaseEntityEnd MACRO

;;
testSpriteImages:
    DB 0x00
testSpriteImageData: ; current image address to draw
    DW 0x0000
testSpriteImageDraw: ; current screen address to draw
    DW 0x0000
; IY => Sprite
; DE => Screen Address
testDrawSprite:
    ld a, (iy + spriteCountImages)
    ld (testSpriteImages), a
    ld h, (iy + imageAddressH)
    ld l, (iy + imageAddressL)
    ld (testSpriteImageData), hl
    ld (testSpriteImageDraw), de

_testDrawSpriteImages:
    ld hl, (testSpriteImageDraw)
    call testDrawBorder

    ld hl, (testSpriteImageData)
    ld de, (testSpriteImageDraw)
    ld b, (iy + imageWidth)
    ld c, (iy + imageHeight)
    call drawImage

    ld hl, (testSpriteImageDraw) ; calculate next position image address
    ld b, 0                      ;
    ld c, (iy + imageWidth)      ;
    inc c                        ;
    add hl, bc                   ;
    ld (testSpriteImageDraw), hl

    ld hl, (testSpriteImageData) ; calculate next image address
    ld b, 0
    ld c, (iy + imageSize)
    add hl, bc
    ld (testSpriteImageData), hl

    ld hl, testSpriteImages
    dec (hl)
    jr nz, _testDrawSpriteImages

    ld hl, (testSpriteImageDraw)
    call testDrawBorder

    ret

; IY => Image
; HL => screen address where draw sprite
; Destroy: A, BC, HL
testDrawBorder:
    dec hl
    call previousLineVideoAddress ; locate init position one line before
    ld b, 1                  ; 1 byte of width
    ld c, (iy + imageHeight) ; lines = image height + 2 lines for border
    inc c                    ;
    inc c
    ld a, 0x66               ;
    jp drawByte

; IX => Entity
; DE => Screen Address
testAnimateSprite:
    ld (ix + entityHexScreenH), d
    ld (ix + entityHexScreenL), e
    ld a, 0x0A
    ld (testAnimateCountLoops), a

_testAnimateSpriteInit:
    call spriteInitializeAnimation

_testAnimateSpriteLoop:
    colorBorder 09
    call spriteUpdateAnimation
    colorBorder 04

    ld a, 0x03
    cp (ix + animationLoops)
    jp z, _testAnimateSpriteEndLoop ; for loop animations look at number of loops

    timerTickTack
    halt
    halt
    halt

    colorBorder 05
    call drawSprite
    colorBorder 04

    ld a, END_FRAME
    cp (ix + animationFrame)
    jp nz, _testAnimateSpriteLoop
_testAnimateSpriteEndLoop:
    ld hl, testAnimateCountLoops
    dec (hl)
    ret z
    jp _testAnimateSpriteInit

testAnimateCountLoops:
    DB 0x00

    INCLUDE "lib/entity.asm"
    INCLUDE "lib/stack.asm"
    INCLUDE "lib/draw.asm"
    INCLUDE "lib/animate.asm"
    INCLUDE "lib/erase.asm"

ENDM

testSprite MACRO _entity, _xPos, _yPos, _sprite, _speed
    ld ix, _entity
IF !NUL _sprite
    ld (ix + entityHexGraphicH), HIGH _sprite
    ld (ix + entityHexGraphicL), LOW _sprite
ENDIF
IF !NUL _speed
    ld (ix + animationSpeed), _speed
ENDIF
    call spriteInitializeAnimation

    ldxy de, _xPos, _yPos
    ld (ix + entityHexScreenH), d
    ld (ix + entityHexScreenL), e

    call testDrawSprite

    ld de, (testSpriteImageDraw)
    inc de
    call testAnimateSprite

ENDM
