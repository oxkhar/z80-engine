; Maximum Horizontal and Vertical Positions on Amstrad CPC
; 
; The Amstrad CPC screen is organized in a grid format, with specific limits for horizontal and vertical positions.
; 
; Maximum Horizontal Position:
; - The screen width is writen 80 bytes, meaning the maximum horizontal position (X) is 79.
; - In mode 1 each byte has 4 pixels to drawm (320), 
; - In mode 0 has 2 pixels (160) ()
; 
; Maximum Vertical Position:
; - The screen height is 25 character lines but we can locate in pixel position
; - Meaning that we have a maximimun of 200 pixels
; 
; Therefore, the valid ranges for screen positions are:
; 
; Horizontal (X): 0 to 79
; Vertical (Y): 0 to 199
; 

PUBLIC .Screen_Address

SCREEN_FREQUENCY EQU 50
SCREEN_ADDRESS EQU 0xC000

; wait until vertical sync happen and take a counter in a timer for each time
; timer take values between 0..49 to know what moment in frequency we are
@Screen_WaitVSync MACRO
    call Screen_WaitVSync
    ;; Tick, tack...
    ld hl, Screen_$frequency    ; [10]
    ld a, [hl]            ; [7]
    inc a                 ; [4]
    cp SCREEN_FREQUENCY   ; [7]
    jr nz, _loadFrequency          ; [12/7]
        xor a             ; [4]
_loadFrequency:
    ld [hl], a            ; [7]
ENDM

.Screen_Address DEFL 0x0000
;; Calculate screen position in memory address for location position in X,Y
@Screen_Address MACRO .x, .y
    .Screen_Address DEFL (SCREEN_ADDRESS + .x + (0x0800 * (.y % 8) + 0x50 * (.y / 8)))
ENDM

@ld_xy MACRO .pairReg, .x, .y
    @Screen_Address .x, .y
    ld .pairReg, .Screen_Address
ENDM

;;
; Modify video address in HL to next line
; HL => Current Address
; Destroy: A, HL
@Screen_NextLineInHL MACRO
    ld a, 0x08    ; [7]
    add a, h      ; [4]
    ld h, a       ; [4]
    jp nc, _end   ; [10] (Better => 25)
        ld a, l       ; [4]
        add a, 0x50   ; [7]
        ld l, a       ; [4]
        ld a, 0xC0    ; [7]
        adc a, h      ; [4]
        ld h, a       ; [4] (Worst => 45)
_end:
ENDM

;;
; Modify video address in DE to next line
; DE => Current Address
; Destroy: A, DE
@Screen_NextLineInDE MACRO
    ld a, 0x08    ; [7]
    add a, d      ; [4]
    ld d, a       ; [4]
    jp nc, _end  ; [10] (Better => 25)
        ld a, e       ; [4]
        add a, 0x50   ; [7]
        ld e, a       ; [4]
        ld a, 0xC0    ; [7]
        adc a, d      ; [4]
        ld d, a       ; [4] (Worst => 45)
_end:
ENDM

;;
; Modify HL video address to previous line
; HL => Current Address
; Destroy: A, HL
@Screen_PreviousLine MACRO
    ld a, h      ;
    sub 0x08     ;
    cp 0xC0      ;
    jp p, _out   ; goto [out]
        add a, 0x40
        ld h, a
        cp 0xF9
        jp p, _next
            ld a, l
            sub 0x50
            ld l, a
            jp nc, _end ; goto end
                sub 0x30
                ld l, a
                ld h, 0xFF
                jp _end    ; goto end
_next:
    ld a, l      ; [next]
    sub 0x50     ;
    ld l, a      ;
    ld a, h
_out:
    sbc a, 0
    ld h, a      ; [out]
_end:
ENDM

