;;
; Animation package constants and macros
; Contains animation-related definitions and utilities

; Package constants
ANIMATION_BOUNCE_STEP    EQU 0
ANIMATION_END_FRAME      EQU 0xFFFF

;;
; Updates sprite and returns if animation not complete
; Parameters:
;   IX = Entity structure pointer
; Modifies:
;   A, F
@Animation_NextSpriteAndReturnIfNotLast MACRO
    call Animation_UpdateSprite                ; Update the sprite
    ld a, [ix + ANIMATION_FRAME]               ; Load current animation frame
    cp ANIMATION_END_FRAME                     ; Compare with end frame
    ret nz                                     ; Return if not at end
ENDM

;;
; Updates sprite and jumps if animation not complete
; Parameters:
;   .address = Jump destination address
;   IX = Entity structure pointer
; Modifies:
;   A, F
@Animation_NextSpriteAndJumpIfNotLast MACRO .address
    call Animation_UpdateSprite                ; Update the sprite
    ld a, [ix + ANIMATION_FRAME]               ; Load current animation frame
    cp ANIMATION_END_FRAME                     ; Compare with end frame
    jp nz, .address                            ; Jump if not at end
ENDM

;;
; Initializes entity with sprite and calls initialization
; Parameters:
;   .sprite = Sprite data address
;   .speed = Animation speed (0-255)
;   IX = Entity structure pointer
; Modifies:
;   A, IY
@Animation_CallInitSprite MACRO .sprite, .speed
    @Animation_SetEntitySprite .sprite, .speed  ; Set up entity sprite
    call Animation_InitializeWithGraphicLoaded  ; Initialize with graphic loaded
ENDM

;;
; Initializes entity with sprite and jumps to initialization
; Parameters:
;   .sprite = Sprite data address
;   .speed = Animation speed (0-255)
;   IX = Entity structure pointer
; Modifies:
;   A, IY
@Animation_JumpInitSprite MACRO .sprite, .speed
    @Animation_SetEntitySprite .sprite, .speed  ; Set up entity sprite
    jp Animation_InitializeWithGraphicLoaded    ; Jump to initialization
ENDM

;;
; Sets up entity sprite data
; Parameters:
;   .sprite = Sprite data address
;   .speed = Animation speed (0-255)
;   IX = Entity structure pointer
; Modifies:
;   A, IY
@Animation_SetEntitySprite MACRO .sprite, .speed
    ld [ix + ENTITY_ADDR_SPRITE_H], HIGH .sprite   ; Store high byte of sprite address
    ld [ix + ENTITY_ADDR_SPRITE_L], LOW  .sprite   ; Store low byte of sprite address
    ld [ix + ANIMATION_SPEED], .speed                ; Set animation speed
    ld iyh, HIGH .sprite                             ; Load high byte into IY
    ld iyl, LOW  .sprite                             ; Load low byte into IY
ENDM
