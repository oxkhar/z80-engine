### Components config selection
## More info on https://gitlab.com/oxkhar/makeup/blob/master/example/Makefile
##
# Merge release (choose one... )
#  values:  git-flow | git-trunk (default)
MKUP_HAS += git-trunk
###
MKUP_INSTALL_PATH = .makeUp
MKUP_URL_INSTALL = https://gitlab.com/oxkhar/makeup/-/raw/master/makeup.sh
$(shell [ ! -e "${MKUP_INSTALL_PATH}" ] && (curl -sSL "${MKUP_URL_INSTALL}" | bash -s install "${MKUP_INSTALL_PATH}"))
include ${MKUP_INSTALL_PATH}/MakeUp.mk
